<?php 
	session_start();
	if ($_SESSION['logado'] != 'S') {
		header('Location: index.php');
	}
require_once('include/functions.php');
require_once('classes/clsmaterias.php');
$ObjMaterias = new materias;
$id = RecebeParametro('id');
if($id){
	$ObjMaterias->id = $id;
	$ObjMaterias->Excluir();
	echo('<script>alert("Registro Excluido com Sucesso !"); location.href="lista-materias.php";</script>');
}
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<title>Sistema de Escala - CEAB -Brasil</title>
	<link rel="stylesheet" href="css/css.css" />
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jquery.quicksearch.js"></script>
</head>
<body>
<?php require_once('topo.php') ?>
	<section class="alinha960">
		<h1><img src="images/setas.png" alt="">Listar Matérias</h1>
		<span class="botaoadicionar"><a href="materias.php"><img src="images/novo-registro.png" alt=""></a></span>
	 <table width="100%" name="tabusu" class="tabelaconteudo" id="tabusu">
	 		<thead>
	 			<tr>
	 				<td>Nome da Matéria</td>
	 				<!--<td>Qtd Aulas Manhã</td>
	 				<td>Qtd Aulas Tarde</td>
	 				<td>Qtd Aulas Noite</td>
	 				<td>Qtd Aulas SabadoM</td>
	 				<td>Qtd Aulas SabadoT</td>
	 				<td>Qtd Aulas EAD</td>-->
	 				<td>Sigla da Matéria</td>
	 				<td>Data de Cadastro</td>
	 				<td>Ação</td>
	 			</tr>
	 		</thead>
	 		<tbody>
	 			<?php  
	 				$ArrMaterias = $ObjMaterias->Listar('','nome ASC','');
	 				if(is_array($ArrMaterias)){
	 					foreach($ArrMaterias as $row){
	 			?>
	 			<tr>
	 				<td><?php echo $row['nome']; ?></td>
	 				<!--<td><?php //echo $row['quantidade_manha']; ?></td>
	 				<td><?php //echo $row['quantidade_tarde']; ?></td>
	 				<td><?php //echo $row['quantidade_noite']; ?></td>
	 				<td><?php //echo $row['quantidade_sabadoM']; ?></td>
	 				<td><?php //echo $row['quantidade_sabadoT']; ?></td>
	 				<td><?php //echo $row['quantidade_ead']; ?></td>-->
	 				<td><?php echo $row['sigla']; ?></td>
	 				<td><?php echo  $row['dataformatada'];?></td>
	 				<td><a href="materias.php?id=<?php echo $row['id']; ?>"><img src="images/editar.png" alt=""><span>editar</span></a> <a href="lista-materias.php?str=excluir&id=<?php echo $row['id']; ?>"><img src="images/excluir.png" alt=""><span>excluir</span></a></td>
	 			</tr>
	 			<?php 
		 				}
		 			}
	 			 ?>
	 		</tbody>
	 	</table>	

	</section>

	<?php require_once('rodape.php'); ?>
</body>
</html>