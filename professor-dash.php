<?php include('header.php'); ?>
	
	<?php 
		$escala = "comissario"; 
		$teacher_id = $_SESSION['user_id'];
		
		$get_month = $_GET['month'];
		$get_year = $_GET['year'];
		
		$result = $m->select(array(
			'table' => 'professores',
			'condition' => "id = '$teacher_id' AND status = 1"
		));
		
		$morning_days 		= split(",", $result[0]['professores']['dias_manha']);
		$afternoon_days 	= split(",", $result[0]['professores']['dias_tarde']);
		$night_days 		= split(",", $result[0]['professores']['dias_noite']);
		$ead_days	 		= split(",", $result[0]['professores']['dias_ead']);
						
	?>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	
	<input type="hidden" id="escala" value="<?php echo $escala; ?>" />

	<section class="alinha960">
		<h1><img src="images/setas.png" alt=""> Bem Vindo(a), <?php echo $_SESSION['nome']; ?></h1> 
	</section>
	<section style="float: left; width:100%;">
<?php
	 $mes = date('m');
	 $ano = date('Y');

	 if($get_month){
		 $mes = $get_month;
	 }
	 
	 if($get_year){
		 $ano = $get_year;
	 }

	 /*if($mes == 12){
	 	$ano=date('Y')+1;
	 	$mes = 1;
	 }else{
	 	$ano=date('Y');
	 	$mes=date('m') + 1;
	 }*/

	 if($mes == 1){
		 $dias=31;
		 $nome="Janeiro";
	 }
	 if($mes == 2){
		 $dias=28;
		 $nome="Fevereiro";
	 }
	 if($mes == 3){
		 $dias=31;
		 $nome="Março";
	 }
	 if($mes == 4){
		 $dias=30;
		 $nome="Abril";
	 }
	 if($mes == 5){
		 $dias=31;
		 $nome="Maio";
	 }
	 if($mes == 6){
		 $dias=30;
		 $nome="Junho";
	 }
	 if($mes == 7){
		 $dias=31;
		 $nome="Julho";
	 }
	 if($mes == 8){
		 $dias=31;
		 $nome="Agosto";
	 }
	 if($mes == 9){
		 $dias=30;
		 $nome="Setembro";
	 }
	 if($mes == 10){
		 $dias=31;
		 $nome="Outubro";
	 }
	 if($mes == 11){
		 $dias=30;
		 $nome="Novembro";
	 }
	 if($mes == 12){
		 $dias=31;
		 $nome="Dezembro";
	 }
	?>
	<h1>
		<select id="select_month" class="select_month">
			<option <?php echo ($mes == 1 ? 'selected="selected"' : "") ;?> value="1">Janeiro</option>
			<option <?php echo ($mes == 2 ? 'selected="selected"' : "") ;?> value="2">Fevereiro</option>
			<option <?php echo ($mes == 3 ? 'selected="selected"' : "") ;?> value="3">Março</option>
			<option <?php echo ($mes == 4 ? 'selected="selected"' : "") ;?> value="4">Abril</option>
			<option <?php echo ($mes == 5 ? 'selected="selected"' : "") ;?> value="5">Maio</option>
			<option <?php echo ($mes == 6 ? 'selected="selected"' : "") ;?> value="6">Junho</option>
			<option <?php echo ($mes == 7 ? 'selected="selected"' : "") ;?> value="7">Julho</option>
			<option <?php echo ($mes == 8 ? 'selected="selected"' : "") ;?> value="8">Agosto</option>
			<option <?php echo ($mes == 9 ? 'selected="selected"' : "") ;?> value="9">Setembro</option>
			<option <?php echo ($mes == 10 ? 'selected="selected"' : "") ;?> value="10">Outubro</option>
			<option <?php echo ($mes == 11 ? 'selected="selected"' : "") ;?> value="11">Novembro</option>
			<option <?php echo ($mes == 12 ? 'selected="selected"' : "") ;?> value="12">Dezembro</option>
		</select>
		 de 
		 <select id="select_year" class="select_year">
			<option <?php echo ($ano == 2015 ? 'selected="selected"' : "") ;?> value="2015">2015</option>
			<option <?php echo ($ano == 2016 ? 'selected="selected"' : "") ;?> value="2016">2016</option>
			<option <?php echo ($ano == 2017 ? 'selected="selected"' : "") ;?> value="2017">2017</option>
		</select>
				
	</h1>
	<table width="100%" border="0" height="100%" class="big-calendar teacher-dash" style="">
	<tr class="header">
	<td align="center" width="3%">DOMINGO</td>
	<td align="center" width="15%">SEGUNDA-FEIRA</td>
	<td align="center" width="15%">TERÇA-FEIRA</td>
	<td align="center" width="15%">QUARTA-FEIRA</td>
	<td align="center" width="15%">QUINTA-FEIRA</td>
	<td align="center" width="15%">SEXTA-FEIRA</td>
	<td align="center" width="15%">SÁBADO</td>
	</tr>
	<?php
	 echo "<tr class='days'>";
	 for($i=1;$i<=$dias;$i++) {
		 $diadasemana = date("w",mktime(0,0,0,$mes,$i,$ano));
		 $weekday = date("l",mktime(0,0,0,$mes,$i,$ano));
		 $cont = 0;
		 
		 $p_dia = sprintf("%02d", $i);
		 $p_mes = sprintf("%02d", $mes);
		 $p_ano = sprintf("%02d", $ano);
		 
		 if($i == 1) {
			 while($cont < $diadasemana) {
				 echo "<td></td>";
				 $cont++;
			 }
	 	}
		 echo "<td class='day-item' height='250' valign='top' data-day='". $i ."' data-month='". date('m') ."' data-year='". date('Y') ."' >";
		 echo '<span style="float:right;"><a href="escala-comissario.php?str=deletardia&dia='.$ano.'-'.$mes.'-'.$i.'" style="text-decoration:none; color:#000;">'.$i.'</a></span><br/>';
		 ?>
		 
		<?php
			 
		if($weekday != "Sunday"){
			 
		?>
		 	<div class="period morning <?php if(in_array("$p_mes/$p_dia/$p_ano",$morning_days)){ echo "day_selected"; } ?>" data-period="manha" data-teacher-id="<?php echo $teacher_id; ?>" data-day="<?php echo "$p_mes/$p_dia/$p_ano"; ?>">
		 	</div>
		 	<div class="period afternoon <?php if(in_array("$p_mes/$p_dia/$p_ano",$afternoon_days)){ echo "day_selected"; } ?>" data-period="tarde" data-teacher-id="<?php echo $teacher_id; ?>" data-day="<?php echo "$p_mes/$p_dia/$p_ano"; ?>">
			 	
		 	</div>
		 	
		 	<?php if($weekday != "Saturday"){ ?>
		 	<div class="period night <?php if(in_array("$p_mes/$p_dia/$p_ano",$night_days)){ echo "day_selected"; } ?>" data-period="noite" data-teacher-id="<?php echo $teacher_id; ?>" data-day="<?php echo "$p_mes/$p_dia/$p_ano"; ?>">
			 	
		 	</div>
		 	<?php } ?>
		 	
		 	<?php if($weekday == "Wednesday"){ ?>
		 	<div class="period ead <?php if(in_array("$p_mes/$p_dia/$p_ano",$ead_days)){ echo "day_selected"; } ?>" data-period="ead" data-teacher-id="<?php echo $teacher_id; ?>" data-day="<?php echo "$p_mes/$p_dia/$p_ano"; ?>">
		 	</div>
		 	<?php } ?>
		 	
		 <?php } ?>
		 
		 <?php	
		 echo "</td>";
		 if($diadasemana == 6) {
		 echo "</tr>";
		 echo "<tr class='days'>";
		 }
	 }
	 echo "</tr>";
?>
</table>
	</section>

	<br style="clear:both;">

	<?php require_once('rodape.php'); ?>
</body>
</html>