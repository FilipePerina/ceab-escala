<?php 
	session_start();
	if ($_SESSION['logado'] != 'S') {
		header('Location: index.php');
	}
require_once('include/functions.php');
require_once('classes/clsturmas.php');
require_once('classes/clsescalacomissario.php');
$ObjTurmas = new turmas;
$ObjEscalaComissario = new escalacomissario;
$html='<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<title>Sistema de Escala - CEAB -Brasil</title>
	<link rel="stylesheet" type="text/css" href="css/css-pdf.css" />
</head>
<body>
	<div id="">
		<h2 style="border: 0;" align="center"><img src="images/logo-ceab.png" alt=""> <br> Escala Comissário de Voo</h2>
	</div>
	<div id="alinha960">';
	 $mes=date('m');

	 if($mes == 12){
	 	$ano=date('Y')+1;
	 	$mes = 1;
	 }else{
	 	$ano=date('Y');
	 	$mes=date('m') + 1;
	 }

	 if($mes == 1){
		 $dias=31;
		 $nome="Janeiro";
	 }
	 if($mes == 2){
		 $dias=28;
		 $nome="Fevereiro";
	 }
	 if($mes == 3){
		 $dias=31;
		 $nome="Março";
	 }
	 if($mes == 4){
		 $dias=30;
		 $nome="Abril";
	 }
	 if($mes == 5){
		 $dias=31;
		 $nome="Maio";
	 }
	 if($mes == 6){
		 $dias=30;
		 $nome="Junho";
	 }
	 if($mes == 7){
		 $dias=31;
		 $nome="Julho";
	 }
	 if($mes == 8){
		 $dias=31;
		 $nome="Agosto";
	 }
	 if($mes == 9){
		 $dias=30;
		 $nome="Setembro";
	 }
	 if($mes == 10){
		 $dias=31;
		 $nome="Outubro";
	 }
	 if($mes == 11){
		 $dias=30;
		 $nome="Novembro";
	 }
	 if($mes == 12){
		 $dias=31;
		 $nome="Dezembro";
	 }
	?>
	<?php
	$html.= '<br/> <h2>'.$nome . " de " . $ano.'</h2> <br/>';
	$html .='<table width="100%" border="0" height="842px">
	<tr>
	<td align="center" width="3%"  style="font-size:12px;">DOMINGO</td>
	<td align="center" width="15%" style="font-size:12px;">SEGUNDA-FEIRA</td>
	<td align="center" width="15%" style="font-size:12px;">TERÇA-FEIRA</td>
	<td align="center" width="15%" style="font-size:12px;">QUARTA-FEIRA</td>
	<td align="center" width="15%" style="font-size:12px;">QUINTA-FEIRA</td>
	<td align="center" width="15%" style="font-size:12px;">SEXTA-FEIRA</td>
	<td align="center" width="15%" style="font-size:12px;">SÁBADO</td>
	</tr>';
	?>
	<?php
	 $html.= "<tr>";
	 for($i=1;$i<=$dias;$i++) {
		 $diadasemana = date("w",mktime(0,0,0,$mes,$i,$ano));
		 $cont = 0;
		 if($i == 1) {
			 while($cont < $diadasemana) {
				 $html.= "<td></td>";
				 $cont++;
			 }
	 	}
		 $html.= "<td height='150' valign='top' style='border:none; background-color:#c6c6c6;'>";
		 $html.= '<span style="float:right;">'.$i.'</span><br/>';
		 	$ObjEscalaComissario->data_aula =  $ano.'-'.$mes.'-'.$i;
		 	$ArrEscalaComissario = $ObjEscalaComissario->ListarEscalaDiaria();
		 	if(is_array($ArrEscalaComissario)){
		 		foreach($ArrEscalaComissario as $row){
		 			$html.= '<span style="text-align:center; font-size:11px; font-weight:bold; padding-left:5px; color:#'.$row['cordaturma'].';"><font color="#'.$row['cordaturma'].'" >'.$row['nometurma'].' - '.$row['siglamateria'].' - '.$row['nomeprofessor'].'</font></span><br/>';
		 		}
		 	}	
		 $html.=  "</td>";
		 if($diadasemana == 6) {
		 $html.=  "</tr>";
		 $html.=  "<tr>";
		 }
	 }
	 $html.=  "</tr>";
$html.='</table>
	</div>
</body>
</html>';
include('html2pdf/html2pdf.class.php');
ob_start();
# Converte o html para pdf.
try
{
	/* Aqui estamos instanciando um novo objeto que irá criar o
	* pdf. Então vamos aos parametros passados:
	* 1º parâmetro: Utilize “P” para exibir o documento no
	* formato retrato e “L” para o formato
	* paisagem.
	* 2º parâmetro: Formato da folha A4, A5.......
	* 3º parâmetro: Caso ocorra alguma exceção durante a
	* conversão. Em qual idioma é para
	* exibir o erro. No caso o idioma escolhido
	* foi o português “pt”.
	* 4º parâmetro: Informe TRUE caso o html de entrada esteja
	* no formato unicode e FALSE caso negativo.
	* 5º parâmetro: Codificação a ser utilizada. ISO-8859-15, UTF-8 ......
	* 6º parâmetro: Margem do documento. Você pode informa um
	* único valor como no exemplo acima.
	* Outra forma é informa um array setando as
	* margens separadamente.: Exemplo:
	* $html2pdf = new HTML2PDF(
	* 'P',
	* 'A4',
	* 'pt',
	* false,
	* 'ISO-8859-15',
	* array(5,5,5,8));
	* Sendo que a primeira posição do array representa a margem esquerda depois
	* topo, direita e rodapé. */
	$html2pdf = new HTML2PDF('P','A4','pt', true, 'UTF-8', array(2,2,2,2));
	# Passamos o html que queremos converte.
	$html2pdf->writeHTML($html);
	/* Exibe o pdf:
	* 1º parãmetro: Nome do arquivo pdf. O nome que você quer dar ao pdf gerado.
	* 2º parâmetro: Tipo de saída:
	I: Abre o pdf gerado no navegador.
	D: Abre a janela para você realizar o download do pdf.
	F: Salva o pdf em alguma pasta do servidor. */
	$html2pdf->Output('escala-comissario.pdf', 'I');
}
catch(HTML2PDF_exception $e)
{
	echo $e;
}
ob_flush();
ob_clean();

?>