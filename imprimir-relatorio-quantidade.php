<?php 
	session_start();
	if ($_SESSION['logado'] != 'S') {
		header('Location: index.php');
	}
	
	

	
	require_once('include/functions.php');
	require_once('classes/clsprofessores.php');
	
	$reportType = $_GET["escala"];
	
	if($reportType == "comissario"){
		require_once('classes/clsescalacomissario.php');
		$ObjEscala = new escalacomissario;
	}else{
		require_once('classes/clsescalacheckin.php');
		$ObjEscala = new escalacheckin;
	}
	
	
	$ObjProfessor = new professores;
	
	
	$idProfessor = RecebeParametro('id');
	$ArrProfessor = $ObjProfessor->Listar('id='.$idProfessor, '','');
	if(is_array($ArrProfessor)){
		$nomeProfessor = $ArrProfessor[0]['nome'];
		$idProfessor = $ArrProfessor[0]['id'];
	}
	$str = RecebeParametro('str');
	if($str == 'periodo'){
		$mes = RecebeParametro('mes');
		$ano = RecebeParametro('ano');

		if($mes == 12){
		 	$ano=date('Y')+1;
		 	$mes = 1;
	 	}else
		 	$ano=date('Y');

		if($mes == 1){
			 $dias=31;
			 $nome="Janeiro";
		 }
		 if($mes == 2){
			 $dias=28;
			 $nome="Fevereiro";
		 }
		 if($mes == 3){
			 $dias=31;
			 $nome="Março";
		 }
		 if($mes == 4){
			 $dias=30;
			 $nome="Abril";
		 }
		 if($mes == 5){
			 $dias=31;
			 $nome="Maio";
		 }
		 if($mes == 6){
			 $dias=30;
			 $nome="Junho";
		 }
		 if($mes == 7){
			 $dias=31;
			 $nome="Julho";
		 }
		 if($mes == 8){
			 $dias=31;
			 $nome="Agosto";
		 }
		 if($mes == 9){
			 $dias=30;
			 $nome="Setembro";
		 }
		 if($mes == 10){
			 $dias=31;
			 $nome="Outubro";
		 }
		 if($mes == 11){
			 $dias=30;
			 $nome="Novembro";
		 }
		 if($mes == 12){
			 $dias=31;
			 $nome="Dezembro";
		 }
		 

	}else{
		$mes = date('m') + 1;
		$ano = date('Y');

		if($mes == 1){
			 $dias=31;
			 $nome="Janeiro";
		 }
		 if($mes == 2){
			 $dias=28;
			 $nome="Fevereiro";
		 }
		 if($mes == 3){
			 $dias=31;
			 $nome="Março";
		 }
		 if($mes == 4){
			 $dias=30;
			 $nome="Abril";
		 }
		 if($mes == 5){
			 $dias=31;
			 $nome="Maio";
		 }
		 if($mes == 6){
			 $dias=30;
			 $nome="Junho";
		 }
		 if($mes == 7){
			 $dias=31;
			 $nome="Julho";
		 }
		 if($mes == 8){
			 $dias=31;
			 $nome="Agosto";
		 }
		 if($mes == 9){
			 $dias=30;
			 $nome="Setembro";
		 }
		 if($mes == 10){
			 $dias=31;
			 $nome="Outubro";
		 }
		 if($mes == 11){
			 $dias=30;
			 $nome="Novembro";
		 }
		 if($mes == 12){
			 $dias=31;
			 $nome="Dezembro";
		 }
	}	 
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<title>Sistema de Escala - CEAB -Brasil</title>
	<link rel="stylesheet" href="css/css.css" />
	<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
	<style>
		body{background-color: #FFF;}
		h1{color:#000;}
	</style>
	<script>
	$(document).ready(function() {
	    $('a#imprimir').click(function() {
	    window.print();
	    return false;
	    });
    });
	</script>
</head>
<body>
	<section class="alinha960">
		<h1>Relatório de quantidade de horas trabalhadas</h1>
		<span class="botaoadicionar"><a href="#" id="imprimir"><img src="images/imprimir-relatorio.png" alt=""></a></span>
	 <form action="imprimir-relatorio-quantidade.php?id=<?php echo $idProfessor; ?>&str=periodo&escala=<?php echo $reportType; ?>" method="post">
		 <table>
	 	<tr>
			<td>Selecione o <strong>Mês</strong> e o <strong>Ano</strong>:</td>
			<td>
				<select name="mes" id="">
					<option value="">Selecione</option>
					<option value="1">Janeiro</option>
					<option value="2">Fevereiro</option>
					<option value="3">Março</option>
					<option value="4">Abril</option>
					<option value="5">Maio</option>
					<option value="6">Junho</option>
					<option value="7">Julho</option>
					<option value="8">Agosto</option>
					<option value="9">Setembro</option>
					<option value="10">Outubro</option>
					<option value="11">Novembro</option>
					<option value="12">Dezembro</option>
				</select>
			</td>
			<td><input type="text" name="ano" value="<?php echo date('Y'); ?>"></td>
			<td><input type="submit" value="Gerar Relatório"></td>
		</tr>
		</form>
			
	
		</table>

		 <table width="100%" name="tabusu" class="tabelaconteudo">
	 		<thead>
	 			<tr>
	 				<td align="left" width="40%"><img src="images/logo-ceab.png" alt=""></td>
	 				<td align="left">Instrutor: <?php echo $nomeProfessor;?></td>
	 			</tr>
	 			<tr>
	 				<td align='Center' colspan="2">Quantidade de aulas no Mês:</td>
	 				
	 			</tr>
	 		</thead>
	 		<tbody>
				<?php 
					$ObjEscala->id_professor = $idProfessor;
					$ArrRelatorioProf = $ObjEscala->ListarDiasProfessor($dias, $mes, $ano);
					if(is_array($ArrRelatorioProf)){
						$quantidadeHoraAula = 0;
						foreach($ArrRelatorioProf as $row){
				 ?>
	 			<tr>
	 				<td align='Center' colspan="2">
	 					<?php
	 						
					 		switch ($row['periodoaula']) {
								case 'Manha':
									$quantidadeHoraAula += 3;
									break;

								case 'Tarde';
									$quantidadeHoraAula += 3;
									break;
								
								case 'Noite';
									$quantidadeHoraAula += 3;
									break;

								case 'Sabado Manhã';
									$quantidadeHoraAula += 3.3;
									break;

								case 'Sabado Tarde';
									$quantidadeHoraAula += 3.3;
									break;

								case 'EAD';
									$quantidadeHoraAula += 3.3;
									break;
							}
						}
					}
							echo $quantidadeHoraAula . ' Horas';
	 					 ?>

	 				</td>
	 			</tr>
	 		</tbody>
		 	</table>
	 	
	</section>
</body>
</html>
