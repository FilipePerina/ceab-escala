<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<title>Sistema de Escala - CEAB -Brasil</title>

	<link rel="apple-touch-icon" sizes="57x57" href="favicons/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="favicons/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="favicons/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="favicons/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="favicons/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="favicons/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="favicons/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="favicons/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="favicons/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
	<link rel="manifest" href="favicons/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="favicons/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<link rel="stylesheet" href="css/css.css" />
	<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
</head>
<body class="login">
	<section class="alinha960">
	<header>
			<div class="arealogin">
				<img src="images/logo-ceab.png" alt="">
				<table width="100%">
					<form id="login-form" method="post">
					<tr height="50">
						<td width="15%" align="left">Usuário:</td>
						<td><input type="text" name="user" required  style="width:200px;"/></td>
					</tr>
					<tr height="40">
						<td width="15%" align="left">Senha:</td>
						<td><input type="password" name="password" required  style="width:200px;"/></td>
					</tr>
					<tr>
						<td colspan="2">
							<div class="login-message">
								Usuário não encontrado!
							</div>
						</td>
					</tr>
					<tr height="100">
						<td colspan="2" align="center"><input type="image" src="images/entrar.png" alt="" style="margin-right: 170px;"></td>
					</tr>
					</form>
				</table>
			</div>
	</header>

	<section>
		<img src="images/aviao-login.png" alt="" class="aviaologin">
	</section>
</section>
	<?php require_once('rodape.php'); ?>
</body>
</html>