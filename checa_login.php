<?php

require_once('classes/clsusuarios.php');

require_once('include/functions.php');

$ObjUsuarios = new usuarios;
$ObjUsuarios->usuario = limpalixo($_POST['usuario']);
$ObjUsuarios->senha =limpalixo($_POST['senha']);
$ArrUsuarios = $ObjUsuarios->logar();


if($ArrUsuarios){
	session_start();

	$ArrQtdAcesso = $ObjUsuarios->retornaAcessos();
	$_SESSION['acessos'] = $ArrQtdAcesso[0]['acessos'];
	$_SESSION['nome'] = $ArrUsuarios[0]['nome'];
	$_SESSION['logado'] = 'S';

	header('Location: home.php');
}else{
	?>
	<script>
		alert('Dados inválidos!');
		window.location.href="index.php";
	</script>
	<?php
}
?>