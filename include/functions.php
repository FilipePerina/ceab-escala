<?php

function limpalixo($var)
{
	// remove palavras que contenham sintaxe sql
	$texto = @preg_replace(sql_regcase("/(from|select|insert|delete|where|drop table|order by|show tables|#|\*|--|\\\\)/"),"",$var);
	$texto = trim($var);//limpa espaços vazio
	$texto = strip_tags($var);//tira tags html e php
	$texto = addslashes($var);//Adiciona barras invertidas a uma string
	return $texto;
}
function dateFormatBrazil($date)
{
//recebe o parâmetro e armazena em um array separado por -
$date = explode('-', $date);

$date = $date[2].'/'.$date[1].'/'.$date[0];

//retorna a string com a data na ordem correta e formatada
return $date;
}
function dateFormatMysql($date)
{
//recebe o parâmetro e armazena em um array separado por -
$date = explode('/', $date);

$date = $date[2].'-'.$date[1].'-'.$date[0];

//retorna a string com a data na ordem correta e formatada
return $date;
}

function converteCategoria($id)
{
	
	if($id)
	{
		switch($id)
		{
		case "1":
			return 'Cães';
			break;
		case "2":
			return 'Gatos';
			break;	
		case "3":
			return 'Aves';
			break;
		case "4":
			return 'Peixes';
			break;
		case "5":
			return 'Repteis';
			break;
		case"6":
			return 'Roedores';
			break;
		}
	}	
	else
	{
		return false;
	}	
}
function ReplaceVirgulaPonto($comando = null)
{
	if($comando)
	{
		$comando = str_replace(",",".",$comando);
		return $comando;
	}	
}

function ReplacePontoVirgula($comando = null)
{
	if($comando)
	{
		$comando = str_replace(".",",",$comando);
		return $comando;	
	}
}
function FormataMoeda($comando = null)
{
	if($comando)
	{
		$comando = number_format($comando,2,',','.');
		return $comando;
	}
}
function RetiraPontoevirgula($comando){
	if($comando){
		$comando = str_replace('.','',$comando);
		$comando = str_replace(',','',$comando);
		return $comando;	
	}
}
function ReplaceEspacoTraco($comando){
	if($comando){
		$comando = str_replace(' ','-',$comando);
		return $comando;	
	}
		
}
function RecebeParametro($comando = ''){	
	if($comando){
		
		if($_GET[$comando])
			return limpalixo($_GET[$comando]);
		else
			return limpalixo($_POST[$comando]);
	}
}

function InputText($nome='', $id='', $classe='', $maxlenght='', $obrigatorio='nao', $valor=''){
		$campo = '<td><input type="text" name="'.$nome.'"';
		if($id)
			$campo.= ' id="'.$id.'"';
		if ($classe)	
			$campo .=' class="'.$classe.'"';
		if ($maxlenght)	
			$campo .= ' maxlength="'.$maxlenght.'"';
		if($obrigatorio == 'sim')	
			$campo .= ' obrig="sim" nome="'.$nome.'"';
		if($valor)
			$campo .=' value="'.$valor.'"';
		$campo .= '/></td>';
		echo $campo;
}

function InputPassword($nome='', $id='', $classe='', $maxlenght='', $obrigatorio='nao', $valor=''){
		$campo = '<td><input type="password" name="'.$nome.'"';
		if($id)
			$campo.= ' id="'.$id.'"';
		if ($classe)	
			$campo .=' class="'.$classe.'"';
		if ($maxlenght)	
			$campo .= ' maxlength="'.$maxlenght.'"';
		if($obrigatorio == 'sim')	
			$campo .= ' obrig="sim" nome="'.$nome.'"';
		if($valor)
			$campo .=' value="'.$valor.'"';
		$campo .= '/></td>';
		echo $campo;
}

function InputRadio($nome='', $id='', $classe='', $checked = '',$obrigatorio='nao', $valor=''){
	$campo = '<input type="radio" name="'.$nome.'"';
	if($id)
		$campo.= ' id="'.$id.'"';
	if ($classe)	
		$campo .=' class="'.$classe.'"';
	if ($checked)	
		$campo .= ' checked="'.$checked.'"';
	if($obrigatorio == 'sim')	
		$campo .= ' obrig="sim" nome="'.$nome.'"';
	if($valor)
		$campo .=' value="'.$valor.'"/>';
	echo $campo;	
}
function TextArea($nome='', $id='', $classe='', $cols, $rows, $valor=''){
	$campo = '<td><textarea name="'.$nome.'"';
	if($id)
		$campo.= ' id="'.$id.'"';
	if ($classe)	
		$campo .=' class="'.$classe.'"';
	if ($cols)	
		$campo .= ' cols="'.$cols.'"';
	if($rows)	
		$campo .= ' rows="'.$rows.'">';
	if($valor)
		$campo .= $valor.'</textarea></td>';
	else
		$campo .='</textarea></td>';			
	echo $campo;	
}
function InputCheckBox($nome='', $id='', $classe='', $checked = '',$obrigatorio='nao', $valor=''){
	$campo = '<td><input type="checkbox" name="'.$nome.'"';
	if($id)
		$campo.= ' id="'.$id.'"';
	if ($classe)	
		$campo .=' class="'.$classe.'"';
	if ($checked)	
		$campo .= ' checked="'.$checked.'"';
	if($obrigatorio == 'sim')	
		$campo .= ' obrig="sim" nome="'.$nome.'"';
	if($valor)
		$campo .=' value="'.$valor.'"/></td>';
	echo $campo;	
}
function InputFile($nome='', $id='', $classe='', $maxlenght='', $obrigatorio='nao', $valor=''){
	$campo = '<td><input type="file" name="'.$nome.'"';
	if($id)
		$campo.= ' id="'.$id.'"';
	if ($classe)	
		$campo .=' class="'.$classe.'"';
	if ($maxlenght)	
		$campo .= ' maxlength="'.$maxlenght.'"';
	if($obrigatorio == 'sim')	
		$campo .= ' obrig="sim" nome="'.$nome.'"';
	if($valor)
		$campo .=' value="'.$valor.'"/></td>';
	echo $campo;

}

?>