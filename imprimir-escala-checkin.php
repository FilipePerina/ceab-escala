<?php 
	session_start();
	if ($_SESSION['logado'] != 'S') {
		header('Location: index.php');
	}
require_once('include/functions.php');
require_once('classes/clsturmas.php');
require_once('classes/clsescalacheckin.php');
$ObjTurmas = new turmas;
$ObjEscalaCheckin = new escalacheckin;
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<title>Sistema de Escala - CEAB -Brasil</title>
	<link rel="stylesheet" href="css/css.css" />
	<link href="css/print.css" rel="stylesheet" media="print" type="text/css" />
	<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<style>
		body{background-color: #FFF;}
		h1{color:#000;}
		table{ background-color: #FFF;}
		td{background-color: #FFF;}
	</style>
	<script>
	function Imprimir(){
		window.print();
	}
	</script>
</head>
<body onload="Imprimir();">
	<section class="alinha960">
		<h1 style="border: 0;" align="center">Escala Check-in</h1>
	</section>
	<section style="width:98%; margin:0 auto;">
<?php
	 $mes=date('m');

	 if($mes == 12){
	 	$ano=date('Y')+1;
	 	$mes = 1;
	 }else{
	 	$ano=date('Y');
	 	$mes=date('m') + 1;
	 }

	 if($mes == 1){
		 $dias=31;
		 $nome="Janeiro";
	 }
	 if($mes == 2){
		 $dias=28;
		 $nome="Fevereiro";
	 }
	 if($mes == 3){
		 $dias=31;
		 $nome="Março";
	 }
	 if($mes == 4){
		 $dias=30;
		 $nome="Abril";
	 }
	 if($mes == 5){
		 $dias=31;
		 $nome="Maio";
	 }
	 if($mes == 6){
		 $dias=30;
		 $nome="Junho";
	 }
	 if($mes == 7){
		 $dias=31;
		 $nome="Julho";
	 }
	 if($mes == 8){
		 $dias=31;
		 $nome="Agosto";
	 }
	 if($mes == 9){
		 $dias=30;
		 $nome="Setembro";
	 }
	 if($mes == 10){
		 $dias=31;
		 $nome="Outubro";
	 }
	 if($mes == 11){
		 $dias=30;
		 $nome="Novembro";
	 }
	 if($mes == 12){
		 $dias=31;
		 $nome="Dezembro";
	 }
	?>
	<?php
	 echo '<br/> <h1>'.$nome . " de " . $ano.'</h1> <br/>';
	?>
	<table width="100%" border="0" height="100%" style="">
	<tr>
	<td align="center" width="3%">DOMINGO</td>
	<td align="center" width="15%">SEGUNDA-FEIRA</td>
	<td align="center" width="15%">TERÇA-FEIRA</td>
	<td align="center" width="15%">QUARTA-FEIRA</td>
	<td align="center" width="15%">QUINTA-FEIRA</td>
	<td align="center" width="15%">SEXTA-FEIRA</td>
	<td align="center" width="15%">SÁBADO</td>
	</tr>
	<?php
	 echo "<tr>";
	 for($i=1;$i<=$dias;$i++) {
		 $diadasemana = date("w",mktime(0,0,0,$mes,$i,$ano));
		 $cont = 0;
		 if($i == 1) {
			 while($cont < $diadasemana) {
				 echo "<td></td>";
				 $cont++;
			 }
	 	}
		 echo "<td height='250' valign='top' style='border:2px solid #000;'>";
		 echo '<span style="float:right;"><a href="#" style="text-decoration:none; color:#000;">'.$i.'</a></span><br/>';
		 	$ObjEscalaCheckin->data_aula =  $ano.'-'.$mes.'-'.$i;
		 	$Arrescalacheckin = $ObjEscalaCheckin->ListarEscalaDiaria();
		 	if(is_array($Arrescalacheckin)){
		 		foreach($Arrescalacheckin as $row){
		 			echo '<span style="text-align:center; font-size:14px; font-weight:bold; padding-left:5px"><font color="#'.$row['cordaturma'].'" >'.$row['nometurma'].' - '.$row['siglamateria'].' - '.$row['nomeprofessor'].'</font></span><br/>';
		 		}
		 	}	
		 echo "</td>";
		 if($diadasemana == 6) {
		 echo "</tr>";
		 echo "<tr>";
		 }
	 }
	 echo "</tr>";
?>
</table>
	</section>
</body>
</html>