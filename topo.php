<header class="topointerna">
	<section class="alinha960">
		<div class="logo">
			<a href="home.php"><img src="images/logo-ceab.png" alt=""></a>
			<p>Sistema de Escalas</p>
		</div>
		<ul class="menu">
			<li><a href="lista-turmas.php"><img src="images/turmas.png" alt=""></a></li>
			<li><a href="lista-materias.php"><img src="images/materias.png" alt=""></a></li>
			<li><a href="lista-professores.php"><img src="images/professor.png" alt=""></a></li>
			<li><a href="escala.php"><img src="images/escala.png" alt=""></a></li>
			<li><a href="relatorios.php"><img src="images/relatorio.png" alt=""></a></li>
			<li><a href="sair.php"><img src="images/sair.png" alt=""></a></li>
		</ul>
	</section>
</header>