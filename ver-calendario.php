<?php include('header.php'); 
				
		$escala = $_GET['escala'];
		$teacher_id = $_GET['teacher_id'];
		$turma_id = $_GET['turma_id'];
		
		if($turma_id){
			$turma_condition = " AND turmas.id = '$turma_id'";
		}else{
			$turma_condition = "";
		}
		
		$get_month = $_GET['month'];
		$get_year = $_GET['year'];
		
		if($teacher_id){
			$result = $m->select(array(
				'table' => 'professores',
				'condition' => "id = '$teacher_id' AND status = 1"
			));
			
			$morning_days 		= split(",", $result[0]['professores']['dias_manha']);
			$afternoon_days 	= split(",", $result[0]['professores']['dias_tarde']);
			$night_days 		= split(",", $result[0]['professores']['dias_noite']);
			$ead_days	 		= split(",", $result[0]['professores']['dias_ead']);
			
			$teacher_condition = " AND professores.id = $teacher_id";
		
		}else{
			$morning_days 		= array();
			$afternoon_days 	= array();
			$night_days 		= array();
			$ead_days	 		= array();
			$teacher_condition 	= "";
		}
		
		$type = ($escala == "comissario" ? 1 : 2);
		$title = ($escala == "comissario" ? "Comissário de Voo" : "Check In");
	?>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	
	<input type="hidden" id="escala" value="<?php echo $escala; ?>" />

	<section class="alinha960">
		<h1><img src="images/setas.png" alt=""> Calendário Geral - <?php echo $title; ?></h1>
		<span class="botaoadicionar"><a href="#" id="imprimir-escala"><img src="images/imprimir-escala.png" alt=""></a></span>
	 <form action="ver-calendario.php" id="teacher_form" method="get">
		 <input type="hidden" name="month" value="<?php echo $get_month; ?>" />
		 <input type="hidden" name="year" value="<?php echo $get_year; ?>" />
		 
		 <table width="100%" name="tabusu" class="tabelaconteudo">
	 		<tbody>
	 			<tr>
	 				<td>Professores:</td>
	 				<td>
	 				<select name="teacher_id" id="teacher_id">
	 					<option value="" selected>Todos os Professores</option>
						<?php
						
						$today = date('Y-m-d');
						
						if($get_month){
							$today = date("Y-$get_month-d");
						}
						
						if($get_year){
							$today = date("$get_year-m-d");
						}
						
						$result = $m->select(array(
							'table' => 'professores',
							'condition' => "status = 1"
						));
												
						foreach($result as $row){
							$row = $row['professores'];						
						?>
	 					<option value="<?php echo $row['id']?>" <?php echo ($row['id'] == $teacher_id ? "selected='selected'" : "") ?>><?php echo $row['nome']; ?></option>
	 					<?php 
	 					}
	 					?>
	 				</select>
	 				<input type="hidden" name="escala" value="<?php echo $_GET['escala']; ?>" />
	 				</td>
	 			</tr>
	 			<tr>
		 			<td>Turmas:</td>
		 			<td>
			 			<select name="turma_id" id="select_turma_id">
	 					<option value="" selected>Todas as Turmas</option>
						<?php
						
						$today = date('Y-m-d');
						
						$result = $m->select(array(
							'table' => 'turmas',
							'condition' => "tipo = '$type' AND data_final >= '$today'"
						));
												
						foreach($result as $row){
							$row = $row['turmas'];						
						?>
	 					<option value="<?php echo $row['id']?>" <?php echo ($row['id'] == $turma_id ? "selected='selected'" : "") ?>><?php echo $row['nome'] .' - '. $row['periodo'].' - SALA ' . $row['sala']; ?></option>
	 					<?php 
	 					}
	 					?>
	 				</select>
		 			</td>
	 			</tr>
	 		</tbody>
	 	</table>		
 	</form>
	 	 
	</section>
	<section style="float: left; width:100%;" class="printThis">
<?php
	 $mes = date('m');
	 $ano = date('Y');

	 if($get_month){
		 $mes = $get_month;
	 }
	 
	 if($get_year){
		 $ano = $get_year;
	 }
	 
	 /*if($mes == 12){
	 	$ano=date('Y')+1;
	 	$mes = 1;
	 }else{
	 	$ano=date('Y');
	 	$mes=date('m') + 1;
	 }*/

	 if($mes == 1){
		 $dias=31;
		 $nome="Janeiro";
	 }
	 if($mes == 2){
		 $dias=28;
		 $nome="Fevereiro";
	 }
	 if($mes == 3){
		 $dias=31;
		 $nome="Março";
	 }
	 if($mes == 4){
		 $dias=30;
		 $nome="Abril";
	 }
	 if($mes == 5){
		 $dias=31;
		 $nome="Maio";
	 }
	 if($mes == 6){
		 $dias=30;
		 $nome="Junho";
	 }
	 if($mes == 7){
		 $dias=31;
		 $nome="Julho";
	 }
	 if($mes == 8){
		 $dias=31;
		 $nome="Agosto";
	 }
	 if($mes == 9){
		 $dias=30;
		 $nome="Setembro";
	 }
	 if($mes == 10){
		 $dias=31;
		 $nome="Outubro";
	 }
	 if($mes == 11){
		 $dias=30;
		 $nome="Novembro";
	 }
	 if($mes == 12){
		 $dias=31;
		 $nome="Dezembro";
	 }
	?>
	<h1>
		<select id="select_month" class="select_month" data-escala="<?php echo $escala; ?>">
			<option <?php echo ($mes == 1 ? 'selected="selected"' : "") ;?> value="1">Janeiro</option>
			<option <?php echo ($mes == 2 ? 'selected="selected"' : "") ;?> value="2">Fevereiro</option>
			<option <?php echo ($mes == 3 ? 'selected="selected"' : "") ;?> value="3">Março</option>
			<option <?php echo ($mes == 4 ? 'selected="selected"' : "") ;?> value="4">Abril</option>
			<option <?php echo ($mes == 5 ? 'selected="selected"' : "") ;?> value="5">Maio</option>
			<option <?php echo ($mes == 6 ? 'selected="selected"' : "") ;?> value="6">Junho</option>
			<option <?php echo ($mes == 7 ? 'selected="selected"' : "") ;?> value="7">Julho</option>
			<option <?php echo ($mes == 8 ? 'selected="selected"' : "") ;?> value="8">Agosto</option>
			<option <?php echo ($mes == 9 ? 'selected="selected"' : "") ;?> value="9">Setembro</option>
			<option <?php echo ($mes == 10 ? 'selected="selected"' : "") ;?> value="10">Outubro</option>
			<option <?php echo ($mes == 11 ? 'selected="selected"' : "") ;?> value="11">Novembro</option>
			<option <?php echo ($mes == 12 ? 'selected="selected"' : "") ;?> value="12">Dezembro</option>
		</select>
		 de 
		 <select id="select_year" class="select_year" data-escala="<?php echo $escala; ?>">
			<option <?php echo ($ano == 2015 ? 'selected="selected"' : "") ;?> value="2015">2015</option>
			<option <?php echo ($ano == 2016 ? 'selected="selected"' : "") ;?> value="2016">2016</option>
			<option <?php echo ($ano == 2017 ? 'selected="selected"' : "") ;?> value="2017">2017</option>
		</select>
				
	</h1>
	<table width="100%" border="0" height="100%" class="big-calendar" style="">
	<tr class="header">
	<td align="center" width="3%">DOMINGO</td>
	<td align="center" width="15%">SEGUNDA-FEIRA</td>
	<td align="center" width="15%">TERÇA-FEIRA</td>
	<td align="center" width="15%">QUARTA-FEIRA</td>
	<td align="center" width="15%">QUINTA-FEIRA</td>
	<td align="center" width="15%">SEXTA-FEIRA</td>
	<td align="center" width="15%">SÁBADO</td>
	</tr>
	<?php
	 echo "<tr class='days'>";
	 for($i=1;$i<=$dias;$i++) {
		 $diadasemana = date("w",mktime(0,0,0,$mes,$i,$ano));
		 $weekday = date("l",mktime(0,0,0,$mes,$i,$ano));
		 $cont = 0;
		 
		 $p_dia = sprintf("%02d", $i);
		 $p_mes = sprintf("%02d", $mes);
		 $p_ano = sprintf("%02d", $ano);
		 
		 if($i == 1) {
			 while($cont < $diadasemana) {
				 echo "<td></td>";
				 $cont++;
			 }
	 	}
		 echo "<td class='day-item' height='250' valign='top' data-day='". $i ."' data-month='". date('m') ."' data-year='". date('Y') ."' >";
		 echo '<span style="float:right;"><a href="escala-comissario.php?str=deletardia&dia='.$ano.'-'.$mes.'-'.$i.'" style="text-decoration:none; color:#000;">'.$i.'</a></span><br/>';
		 ?>
		 
		<?php
			 
		if($weekday != "Sunday"){
			 
		?>
		 	<div class="period morning <?php if(in_array("$p_mes/$p_dia/$p_ano",$morning_days)){ echo "day_selected"; } ?>" data-period="Manha" data-aula-date="<?php echo "$ano-$mes-$i 00:00:00"; ?>">
			 	<?php 
				 	
				 	$query = "
				 		SELECT * FROM escala_$escala INNER JOIN turmas
				 		ON escala_$escala.id_turma = turmas.id
				 		
				 		INNER JOIN professores
				 		ON escala_$escala.id_professor = professores.id	
				 		
				 		INNER JOIN materias
				 		ON escala_$escala.id_materia = materias.id
				 		
				 		WHERE escala_$escala.data_aula = '$ano-$mes-$i 00:00:00' AND escala_$escala.periodo = 'Manha' AND professores.status = 1 $teacher_condition	$turma_condition
				 	";
				 					 	
				 	$result = $m->query($query);
				 	
				 	if($result){
					 	for($r = 0; $r < count($result); $r++){
					 		$row   = $result[$r];			 	
							$es = $row["escala_$escala"];
							$teacher = $row["professores"];
							$materias = $row["materias"];
					 		$turma = $row["turmas"];

						 
						?>
							
							<div class="teacher" data-escala-id="<?php echo $es['id']; ?>" data-id="<?php echo $teacher['id']; ?>" data-morning="<?php echo $teacher['dias_manha']; ?>" data-afternoon="<?php echo $teacher['dias_tarde']; ?>" data-night="<?php echo $teacher['dias_noite']; ?>" data-ead="<?php echo $teacher['ead']; ?>" style="background: #<?php echo $turma['corturma']; ?>">
								<img src="<?php echo ($teacher['foto'] ? "images/media/" . $teacher['foto'] : "images/sem-foto.png"); ?>"><br>
								<p class="hide-print"><?php echo $turma['nome']; ?> -
<small>(<?php echo $materias["sigla"]; ?>) - <?php echo $teacher['nome']; ?></small>
								</p>
								<p class="show-print" style="color: <?php echo ($turma['corturma'] ? '#'.$turma['corturma'] : '#000') ?>"><?php echo $turma['nome']; ?> -
<small>(<?php echo $materias["sigla"]; ?>) - <?php echo $teacher['nome']; ?></small>
								</p>
								<div class="teacher-menu delete">
									<a href="#" class="delete-escala" data-escala-id="<?php echo $es['id']; ?>"><i class="fa fa-trash"></i></a>
								</div>
							</div>
							
						<?php 
					 	}
				 	}
			 	?>
		 	</div>
		 	<div class="period afternoon <?php if(in_array("$p_mes/$p_dia/$p_ano",$afternoon_days)){ echo "day_selected"; } ?>" data-period="Tarde" data-aula-date="<?php echo "$ano-$mes-$i 00:00:00"; ?>">
			 	<?php 
				 	
				 	$query = "
				 		SELECT * FROM escala_$escala INNER JOIN turmas
				 		ON escala_$escala.id_turma = turmas.id
				 		
				 		INNER JOIN professores
				 		ON escala_$escala.id_professor = professores.id	
				 		
				 		INNER JOIN materias
				 		ON escala_$escala.id_materia = materias.id
				 		
				 		WHERE escala_$escala.data_aula = '$ano-$mes-$i 00:00:00' AND escala_$escala.periodo = 'Tarde' AND professores.status = 1 $teacher_condition $turma_condition
				 	";
				 					 	
				 	$result = $m->query($query);
				 	
				 	if($result){
					 	for($r = 0; $r < count($result); $r++){
					 		$row   = $result[$r];			 	
					 		$es = $row["escala_$escala"];
					 		$teacher = $row["professores"];
					 		$materias = $row["materias"];
					 		$turma = $row["turmas"];
						 
						?>
							
							<div class="teacher" data-escala-id="<?php echo $es['id']; ?>" data-id="<?php echo $teacher['id']; ?>" data-morning="<?php echo $teacher['dias_manha']; ?>" data-afternoon="<?php echo $teacher['dias_tarde']; ?>" data-night="<?php echo $teacher['dias_noite']; ?>" data-ead="<?php echo $teacher['ead']; ?>" style="background: #<?php echo $turma['corturma']; ?>">
								<img src="<?php echo ($teacher['foto'] ? "images/media/" . $teacher['foto'] : "images/sem-foto.png"); ?>"><br>
								<p class="hide-print"><?php echo $turma['nome']; ?> -
<small>(<?php echo $materias["sigla"]; ?>) - <?php echo $teacher['nome']; ?></small>
								</p>
								<p class="show-print" style="color: <?php echo ($turma['corturma'] ? '#'.$turma['corturma'] : '#000') ?>"><?php echo $turma['nome']; ?> -
<small>(<?php echo $materias["sigla"]; ?>) - <?php echo $teacher['nome']; ?></small>
								</p>
								<div class="teacher-menu delete">
									<a href="#" class="delete-escala" data-escala-id="<?php echo $es['id']; ?>"><i class="fa fa-trash"></i></a>
								</div>
							</div>
							
						<?php 
					 	}
				 	}
			 	?>
		 	</div>
		 	
		 	<?php if($weekday != "Saturday"){ ?>
		 	<div class="period night <?php if(in_array("$p_mes/$p_dia/$p_ano",$night_days)){ echo "day_selected"; } ?>" data-period="Noite" data-aula-date="<?php echo "$ano-$mes-$i 00:00:00"; ?>">
			 	<?php 
				 	
				 	$query = "
				 		SELECT * FROM escala_$escala INNER JOIN turmas
				 		ON escala_$escala.id_turma = turmas.id
				 		
				 		INNER JOIN professores
				 		ON escala_$escala.id_professor = professores.id	
				 		
				 		INNER JOIN materias
				 		ON escala_$escala.id_materia = materias.id
				 		
				 		WHERE escala_$escala.data_aula = '$ano-$mes-$i 00:00:00' AND escala_$escala.periodo = 'Noite' AND professores.status = 1 $teacher_condition	$turma_condition
				 	";
				 					 	
				 	$result = $m->query($query);
				 	
				 	if($result){
					 	for($r = 0; $r < count($result); $r++){
					 		$row   = $result[$r];			 	
							$es = $row["escala_$escala"];
							$teacher = $row["professores"];
							$materias = $row["materias"];
					 		$turma = $row["turmas"];

						 
						?>
							
							<div class="teacher" data-escala-id="<?php echo $es['id']; ?>" data-id="<?php echo $teacher['id']; ?>" data-morning="<?php echo $teacher['dias_manha']; ?>" data-afternoon="<?php echo $teacher['dias_tarde']; ?>" data-night="<?php echo $teacher['dias_noite']; ?>" data-ead="<?php echo $teacher['ead']; ?>" style="background: #<?php echo $turma['corturma']; ?>">
								<img src="<?php echo ($teacher['foto'] ? "images/media/" . $teacher['foto'] : "images/sem-foto.png"); ?>"><br>
								<p class="hide-print"><?php echo $turma['nome']; ?> -
<small>(<?php echo $materias["sigla"]; ?>) - <?php echo $teacher['nome']; ?></small>
								</p>
								<p class="show-print" style="color: <?php echo ($turma['corturma'] ? '#'.$turma['corturma'] : '#000') ?>"><?php echo $turma['nome']; ?> -
<small>(<?php echo $materias["sigla"]; ?>) - <?php echo $teacher['nome']; ?></small>
								</p>
								<div class="teacher-menu delete">
									<a href="#" class="delete-escala" data-escala-id="<?php echo $es['id']; ?>"><i class="fa fa-trash"></i></a>
								</div>
							</div>
							
						<?php 
					 	}
				 	}
			 	?>
		 	</div>
		 	<?php } ?>
		 	
		 	<?php if($weekday == "Wednesday"){ ?>
		 	<div class="period ead <?php if(in_array("$p_mes/$p_dia/$p_ano",$night_days)){ echo "day_selected"; } ?>" data-period="EAD" data-aula-date="<?php echo "$ano-$mes-$i 00:00:00"; ?>">
			 	<?php 
				 	
				 	$query = "
				 		SELECT * FROM escala_$escala INNER JOIN turmas
				 		ON escala_$escala.id_turma = turmas.id
				 		
				 		INNER JOIN professores
				 		ON escala_$escala.id_professor = professores.id	
				 		
				 		INNER JOIN materias
				 		ON escala_$escala.id_materia = materias.id
				 		
				 		WHERE escala_$escala.data_aula = '$ano-$mes-$i 00:00:00' AND escala_$escala.periodo = 'EAD' AND professores.status = 1 $teacher_condition $turma_condition
				 	";
				 					 	
				 	$result = $m->query($query);
				 	
				 	if($result){
					 	for($r = 0; $r < count($result); $r++){
					 		$row   = $result[$r];			 	
							$es = $row["escala_$escala"];
							$teacher = $row["professores"];
							$materias = $row["materias"];
					 		$turma = $row["turmas"];

						 
						?>
							
							<div class="teacher" data-escala-id="<?php echo $es['id']; ?>" data-id="<?php echo $teacher['id']; ?>" data-morning="<?php echo $teacher['dias_manha']; ?>" data-afternoon="<?php echo $teacher['dias_tarde']; ?>" data-night="<?php echo $teacher['dias_noite']; ?>" data-ead="<?php echo $teacher['ead']; ?>" style="background: #<?php echo $turma['corturma']; ?>">
								<img src="<?php echo ($teacher['foto'] ? "images/media/" . $teacher['foto'] : "images/sem-foto.png"); ?>"><br>
								<p class="hide-print"><?php echo $turma['nome']; ?> -
<small>(<?php echo $materias["sigla"]; ?>) - <?php echo $teacher['nome']; ?></small>
								</p>
								<p class="show-print" style="color: <?php echo ($turma['corturma'] ? '#'.$turma['corturma'] : '#000') ?>"><?php echo $turma['nome']; ?> -
<small>(<?php echo $materias["sigla"]; ?>) - <?php echo $teacher['nome']; ?></small>
								</p>
								<div class="teacher-menu delete">
									<a href="#" class="delete-escala" data-escala-id="<?php echo $es['id']; ?>"><i class="fa fa-trash"></i></a>
								</div>
							</div>
							
						<?php 
					 	}
				 	}
			 	?>
		 	</div>
		 	<?php } ?>
		 	
		 <?php } ?>
		 
		 <?php	
		 echo "</td>";
		 if($diadasemana == 6) {
		 echo "</tr>";
		 echo "<tr class='days'>";
		 }
	 }
	 echo "</tr>";
?>
</table>
	</section>

	<br style="clear:both;">

	<?php require_once('rodape.php'); ?>
</body>
</html>
