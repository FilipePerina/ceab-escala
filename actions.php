<?php
require "core.php";

function login($post){
	global $m;
	global $c;
	
	$allowed_ips = array("187.11.239.189", "127.0.0.1", "::1");
	
	$user = $post['user'];
	$password = $post['password'];
	
	// First check if user exist
	/*$result = $c->select(array(
		'table' => 'tb_admin_users',
		'condition' => "login = '$user' AND senha = '$password'"
	));*/
	
	$result = $m->select(array(
		'table' => 'usuarios',
		'condition' => "login = '$user' AND senha = '$password'"
	));
	
	if($result){
		
		if(!in_array($_SERVER['REMOTE_ADDR'], $allowed_ips) || $result[0]['usuarios']['id'] == 1){
			$_SESSION['acessos'] = 0; //$result[0]['tb_admin_users']['acessos'];
			/*$_SESSION['nome'] = $result[0]['tb_admin_users']['name'];
			$_SESSION['user_id'] = $result[0]['tb_admin_users']['id'];*/
			
			$_SESSION['nome'] = $result[0]['usuarios']['nome'];
			$_SESSION['user_id'] = $result[0]['usuarios']['id'];
			
			$_SESSION['logado'] = 'S';
			$_SESSION['type'] = 'admin';
			$_SESSION['menu_access'] = 'admin';
			
			$response = array(
				'status' => 1,
				'user' => 'admin'
			);
		}else{
			$response = array(
				'status' => 0,
				'message' => "Você não tem permissão para acessar deste local."
			);
		}
		
		echo json_encode($response);
				
	}else{ // If User not found, try Teachers
		
		
		$result = $m->select(array(
			'table' => 'professores',
			'condition' => "email = '$user' AND senha = '$password' AND status = 1"
		));
		
		if($result){
			
			
			if(!in_array($_SERVER['REMOTE_ADDR'], $allowed_ips)){
			$_SESSION['acessos'] = $result[0]['usuarios']['acessos'];
			$_SESSION['nome'] = $result[0]['professores']['nome'];
			$_SESSION['user_id'] = $result[0]['professores']['id'];
			$_SESSION['logado'] = 'S';
			$_SESSION['type'] = 'teacher';
			$_SESSION['menu_access'] = 'teacher';
			
			$response = array(
				'status' => 1,
				'user' => 'teacher'
			);
			
			}else{
				$response = array(
					'status' => 0,
					'message' => "Você não tem permissão para acessar deste local. " . $_SERVER['REMOTE_ADDR']
				);
			}
			
			echo json_encode($response);
			
			
			
		}else{
			$response = array(
				'status' => 0,
				'message' => "Usuário Não Encontrado!"
			);
			
			echo json_encode($response);
		}
		
	}
	
}

function teacherStatus($id, $status){
	global $m;
	
	$data = array(
		'status' => $status,
	);
	
	$result = $m->update('professores',$data,"id = '$id'");
	
}

function teacherDaySelected($teacher_id, $day, $period){
	global $m;
	$list = split(",", $m->get('professores',"dias_$period","id = $teacher_id"));
	
	if(in_array($day, $list)){
		$day_arr = array($day);
		$list = array_diff($list, $day_arr);
	}else{
		array_push($list, $day);
	}
	
	$list = implode(",",$list);
	
	$data = array(
		"dias_$period" => $list,
	);
	$result = $m->update('professores',$data,"id = $teacher_id");	
}

function listClass(){
	global $m;
	
	$schedule = $_POST['schedule'];
	$today = date('Y-m-d H:i:s');
	
	$result = $m->select(array(
		'table' => 'turmas',
		'condition' => "data_final > '$today' AND tipo = '$schedule'"
	));
		
	echo json_encode($result);
	
}

function listTeachers(){
	global $m;
	
	$result = $m->select(array(
		'table' => 'professores',
	));
		
	echo json_encode($result);
	
}

function getClass($id){
	global $m;
	 	
	$result = $m->select(array(
		'table' => 'turmas',
		'condition' => "id = '$id'"
	));
		
	echo json_encode($result);						
}

function getTeachers($id){
	global $m;
	 	
	$result = $m->select(array(
		'table' => 'professores_rel_materias',
		'condition' => "id_materia = '$id'"
	));
		
	echo json_encode($result);						
}

function addEscala($escala, $id_turma, $id_professor, $id_materia, $data_aula, $periodo){
	global $m;
	
	$data = array(
		'id_turma' => $id_turma,
		'id_professor' => $id_professor,
		'id_materia' => $id_materia,
		'data_aula' => $data_aula,
		'data_insercao' => date('Y-m-d H:i:s'),
		'periodo' => $periodo
	);
	
	$result = $m->insert("escala_$escala", $data);
	$last_id = mysql_insert_id();
	
	$teacher_photo = $name = $m->get('professores','foto',"id = $id_professor");
	
	if($result){
		
		
		$turma_name 		= $m->get("turmas","nome","id='$id_turma'");
		$materia_name 		= $m->get("materias","nome","id='$id_materia'");
		$professor_name 	= $m->get("professores","nome","id='$id_professor'");
		$sala_no			= $m->get("turmas","sala","id='$id_turma'");
		
	?>
	
	
	<div class="teacher" data-escala-id="<?php echo $last_id; ?>" data-id="<?php echo $id_professor; ?>" data-morning="<?php echo $m->get("professores","dias_manha","id = '$id_professor'"); ?>" data-afternoon="<?php echo $m->get("professores","dias_tarde","id = '$id_professor'"); ?>" data-night="<?php echo $m->get("professores","dias_noite","id = '$id_professor'"); ?>" data-ead="<?php echo $m->get("professores","dias_ead","id = '$id_professor'"); ?>" style="background: #<?php echo $m->get("turmas","corturma","id = '$id_turma'"); ?>" data-trigger="focus" data-toggle="popover" data-placement="top" data-html="true" data-content="<strong>Matéria:</strong> <?php echo $materias['nome'] . " (" . $materia_name . ")"; ?><br/><strong>Sala:</strong> <?php echo $sala_no ?>" >
		<img src="<?php echo ($teacher_photo ? "images/media/" . $teacher_photo : "images/sem-foto.png"); ?>"><br>
		<p class="hide-print"><?php echo $m->get("turmas","nome","id='$id_turma'"); ?> - <br/>
		<small>(<?php echo $m->get("materias","sigla","id='$id_materia'"); ?>) - <?php echo $m->get("professores","nome","id='$id_professor'"); ?></small>
		</p>
		<p class="show-print"><?php echo $m->get("turmas","nome","id='$id_turma'"); ?> - <br/>
		<small>(<?php echo $m->get("materias","sigla","id='$id_materia'"); ?>) - <?php echo $m->get("professores","nome","id='$id_professor'"); ?></small>
		</p>
		<div class="teacher-menu delete">
			<a href="#" class="delete-escala" data-escala-id="<?php echo $last_id; ?>"><i class="fa fa-trash"></i></a>
		</div>
	</div>
	
	
	<?php
	}else{
		echo mysql_error();
	}
	
}	

function deleteEscala($escala, $id){
	global $m;
	$m->delete("escala_$escala","id = $id");
}	

function saveTeacherDates($teacher_id, $periodo, $dias){
	global $m;
	
	$data = array(
		$periodo => $dias,
	);
	
	$result = $m->update("professores", $data, "id = $teacher_id");
	
	if($result){
		echo 1;
	}else{
		echo mysql_error();
	}
}							

$act = $_REQUEST['act'];

// Login
if($act == 'login'){ 	login($_POST); }

// Teacher List
if($act == 'teacher-status'){	teacherStatus($_POST['id'], $_POST['status']);	}

// Teacher Dashboard
if($act == 'day-select'){		teacherDaySelected($_POST['teacher_id'], $_POST['day'], $_POST['period']);	}

// Calendar Actions
if($act == 'list-classes'){ 	listClass(); }
if($act == 'list-teachers'){	listTeachers();	}
if($act == 'get-class'){		getClass($_POST['id']);	}
if($act == 'get-teachers'){		getTeachers($_POST['id']);	}
if($act == 'add-escala'){		addEscala($_POST['escala'], $_POST['id_turma'], $_POST['id_professor'], $_POST['id_materia'], $_POST['data_aula'], $_POST['periodo']);	}
if($act == 'delete-escala'){	deleteEscala($_POST['escala'], $_POST['id']);	}
if($act == 'save-teacher-dates'){	saveTeacherDates($_POST['teacher_id'], $_POST['periodo'], $_POST['dias']);	}
