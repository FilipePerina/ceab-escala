<?php
	session_start();
	if ($_SESSION['logado'] != 'S') {
		header('Location: index.php');
	}
require_once('include/functions.php');
require_once('classes/clsprofessores.php');
$ObjProfessor = new professores;
$str = RecebeParametro('str');
if($str === 'zerarprofessor'){
	echo '<script>
		var a =	confirm("Deseja zerar os dias dos professores ?");
		if(a == true){
			location.href="home.php?str=zerar";
		}else{
			location.href="home.php";
		}
		</script>';
}elseif($str === 'zerar'){
	$ObjProfessor->ZerarProfessores();
	echo'<script> alert("Dias dos professores Zerados !");</script>';
	header('Location: home.php');
}
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<title>Sistema de Escala - CEAB - Brasil</title>
	
	<link rel="apple-touch-icon" sizes="57x57" href="favicons/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="favicons/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="favicons/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="favicons/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="favicons/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="favicons/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="favicons/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="favicons/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="favicons/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
	<link rel="manifest" href="favicons/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="favicons/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	
	<link rel="stylesheet" href="css/css.css" />
	<style type="text/css">
.zerar {
	-moz-box-shadow:inset 0px 1px 0px 0px #c1ed9c;
	-webkit-box-shadow:inset 0px 1px 0px 0px #c1ed9c;
	box-shadow:inset 0px 1px 0px 0px #c1ed9c;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #9dce2c), color-stop(1, #8cb82b) );
	background:-moz-linear-gradient( center top, #9dce2c 5%, #8cb82b 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#9dce2c', endColorstr='#8cb82b');
	background-color:#9dce2c;
	-webkit-border-top-left-radius:6px;
	-moz-border-radius-topleft:6px;
	border-top-left-radius:6px;
	-webkit-border-top-right-radius:6px;
	-moz-border-radius-topright:6px;
	border-top-right-radius:6px;
	-webkit-border-bottom-right-radius:6px;
	-moz-border-radius-bottomright:6px;
	border-bottom-right-radius:6px;
	-webkit-border-bottom-left-radius:6px;
	-moz-border-radius-bottomleft:6px;
	border-bottom-left-radius:6px;
	text-indent:0;
	border:1px solid #83c41a;
	display:inline-block;
	color:#ffffff;
	font-family:'Droid Sans';
	font-size:14px;
	font-weight:normal;
	font-style:normal;
	height:65px;
	line-height:65px;
	width:190px;
	text-decoration:none;
	text-align:center;
	text-shadow:1px 2px 0px #689324;
}
.zerar:hover {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #8cb82b), color-stop(1, #9dce2c) );
	background:-moz-linear-gradient( center top, #8cb82b 5%, #9dce2c 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#8cb82b', endColorstr='#9dce2c');
	background-color:#8cb82b;
}.zerar:active {
	position:relative;
	top:1px;
}</style>
</head>
<body>
<?php require_once('topo.php') ?>
	<section class="alinha960">
		<h1><img src="images/setas.png" alt=""><span>Olá</span> <?php echo $_SESSION['nome']; ?>,</h1>
		<p>você acessou o sistema <?php echo $_SESSION['acessos']; ?> vezes.</p>
		<br clear="all">
		<a href="home.php?str=zerarprofessor" class="zerar">Apagar dias dos professores</a>
	</section>

	<?php require_once('rodape.php'); ?>
</body>
</html>