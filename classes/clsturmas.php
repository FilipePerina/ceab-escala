<?php 
	require_once('mysql.php');
	class turmas{
		var $id = 0;
		var $nome = "";
		var $periodo = "";
		var $data_inicial = "";
		var $data_final = "";
		var $tipo = 0;
		var $corturma = "";
		var $sala = "";
		var $data_insercao="";
		var $data_alteracao = "";
	
	function Inserir(){
		$banco = new Sql;
		$sql = 'Insert into turmas(nome, periodo, data_inicial, data_final, tipo, corturma, sala, data_insercao) VALUES';
		$sql .= '("'.$this->nome.'","'.$this->periodo.'","'.$this->data_inicial.'", "'.$this->data_final.'", '.$this->tipo.', "'.$this->corturma.'",'.$this->sala.',"'.$this->data_insercao.'")';
		/*echo $sql;
		exit();*/
		$banco->Query($sql);
		$banco->Close();
	}
	function Alterar(){
		if($this->id){
			$banco = new Sql;
			$sql ='UPDATE turmas SET';
			$sql .= ' nome ="'.$this->nome.'"';
			$sql .= ', periodo ="'.$this->periodo.'"';
			$sql .= ', data_inicial ="'.$this->data_inicial.'"';
			$sql .= ', data_final ="'.$this->data_final.'"';
			$sql .= ', tipo ='.$this->tipo;
			$sql .= ', corturma ="'.$this->corturma.'"';
			$sql .= ', sala ='.$this->sala;
			$sql .= ', data_alteracao ="'.$this->data_alteracao.'"';
			$sql .= ' WHERE id ='.$this->id;
			/*echo $sql;
			exit();*/
			$banco->Query($sql);
			$banco->Close();
		}	

	}
	function Excluir(){
		if($this->id){
			$banco = new Sql;
			$sql = 'DELETE FROM turmas where id='.$this->id;
			$banco->Query($sql);
			$banco->Close();

		}
	}
	function Listar($where = '', $orderby = '', $limit =''){
		$banco = new Sql;
		$sql = 'SELECT *,DATE_FORMAT(data_inicial, "%d/%m/%Y") as datainicial, DATE_FORMAT(data_final, "%d/%m/%Y") as datafinal FROM turmas';
		if($where)
			$sql .= ' WHERE '.$where;	
		if($orderby)
			$sql .= ' ORDER BY ' .$orderby;
		if($limit)
			$sql .=' LIMIT '.$limit;
		/*echo $sql;
		exit();*/
		$banco->Query($sql);
		$ArrSql = $banco->SelecionaBanco($banco->getRecordSet());
		$banco->Close();
		if($ArrSql)
			return $ArrSql;
		else
			return false;
	}
}

?>