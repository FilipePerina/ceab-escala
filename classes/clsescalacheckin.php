<?php 
	require_once('mysql.php');
	require_once('clsmaterias.php');
	require_once('clsturmas.php');
	class escalacheckin{
		var $id = 0;
		var $id_professor=0;
		var $id_materia =0;
		var $id_turma =0;
		var $data_aula = "";
		var $periodo ="";
		var $data_insercao ="";
		var $data_alteracao ="";

	function Inserir(){
		$banco = new Sql;
		$sql ='INSERT INTO escala_checkin(id_turma, id_professor, id_materia, data_aula, periodo, data_insercao) VALUES';
		$sql.='('.$this->id_turma.','.$this->id_professor.', '.$this->id_materia.', "'.$this->data_aula.'", "'.$this->periodo.'", "'.$this->data_insercao.'")';
		$banco->Query($sql);
		$banco->Close();
		/* Vamos subtrair a quantidade de aulas disponiveis no dia e no perido da matéria ! */
		/*$ObjMaterias = new materias;
		$ObjTurmas = new turmas;
		$ArrTurmas = $ObjTurmas->Listar('id='.$this->id_turma);
		if(is_array($ArrTurmas)){
			$periodo = $ArrTurmas[0]['periodo'];
			$ObjMaterias->SubtraiQuantidade($periodo, $this->id_materia);
		}*/
	}	
	/*function Alterar(){
		if($this->id){
			$banco = new Sql;
			$sql ='UPDATE professores SET';
			$sql .= ' nome ="'.$this->nome.'"';
			$sql .= ', dias_disponiveis ="'.$this->dias_disponiveis.'"';
			$sql .= ', email ="'.$this->email.'"';
			$sql .= ', data_alteracao ="'.$this->data_alteracao.'"';
			$sql .= ' WHERE id ='.$this->id;
			/*echo $sql;
			exit();*/
			/*$banco->Query($sql);
			$banco->Close();
		}	

	}
	function Excluir(){
		if($this->id){
			$banco = new Sql;
			$sql = 'DELETE FROM professores where id='.$this->id;
			$banco->Query($sql);
			$banco->Close();

		}
	}*/
	function Listar($where = '', $orderby = '', $limit =''){
		$banco = new Sql;
		$sql = 'SELECT *,DATE_FORMAT(data_insercao, "%d/%m/%Y") as dataformatada FROM escala_checkin';
		if($where)
			$sql .= ' WHERE '.$where;	
		if($orderby)
			$sql .= ' ORDER BY ' .$orderby;
		if($limit)
			$sql .=' LIMIT '.$limit;
		/*echo $sql;
		exit();*/
		$banco->Query($sql);
		$ArrSql = $banco->SelecionaBanco($banco->getRecordSet());
		$banco->Close();
		if($ArrSql)
			return $ArrSql;
		else
			return false;
	}
	function ListarEscalaDiaria(){
		if($this->data_aula){
			$banco = new Sql;
			$sql = 'SELECT B.nome as nometurma, B.corturma as cordaturma, B.sala as salaturma, C.nome as nomeprofessor, D.sigla as siglamateria FROM escala_checkin as A, turmas as B, professores as C, materias as D WHERE ';
			$sql .=' A.id_turma = B.id and A.id_professor = C.id and A.id_materia = D.id AND A.data_aula="'.$this->data_aula.'" ORDER BY A.id ASC';
			/*echo $sql;
			exit();*/
			$banco->Query($sql);
			$ArrSql = $banco->SelecionaBanco($banco->getRecordSet());
			$banco->Close();
			if($ArrSql)
				return $ArrSql;
			else
				return false;
		}	

	}
	function ListarDiasProfessor($dias='',$mes='',$ano=''){
		$datacompletainicio = $ano.'-'.$mes.'-01';
		$datacompletafim = $ano.'-'.$mes.'-'.$dias;
		if($this->id_professor){
			$banco = new Sql;
			$sql = 'SELECT DATE_FORMAT( A.data_aula , "%d/%m/%Y")as dataaula, A.periodo as periodoaula, B.nome as nometurma, C.nome as nomemateria FROM escala_checkin as A, turmas as B, materias as C WHERE';
			$sql .= ' B.id = A.id_turma AND A.id_materia = C.id AND A.id_professor ='.$this->id_professor;
			$sql .= ' AND A.data_aula >="'.$datacompletainicio.'" AND A.data_aula <="'.$datacompletafim.'"';
			$sql .= ' ORDER BY A.data_aula ASC';
			$banco->Query($sql);
			$ArrSql = $banco->SelecionaBanco($banco->getRecordSet());
			$banco->Close();
			if(is_array($ArrSql))
				return $ArrSql;
			else
				return false;
		}
	}
	function DeletarDia(){
		if($this->data_aula){
			$ObjMaterias = new materias;
			$banco = new Sql;
			/* Pesquisa de todas as materias no dia e adiciona mais 1 na quantidade de materia */
			$sql = 'SELECT * from escala_checkin WHERE data_aula ="'.$this->data_aula.'"';
			$banco->Query($sql);
			$ArrSql = $banco->SelecionaBanco($banco->getRecordSet());
			if(is_array($ArrSql)){
				/* Vamos adicionar a quantidade de aulas disponiveis no dia e no perido da matéria ! */
				foreach($ArrSql as $row){
					$ObjMaterias->AdicionaQuantidade($row['periodo'], $row['id_materia']);
				}
			}
			$sql ='DELETE FROM escala_checkin WHERE data_aula ="'.$this->data_aula.'"';
			$banco->Query($sql);
			$banco->Close();
		}
	}
}

?>