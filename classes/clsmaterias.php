<?php 
	require_once('mysql.php');
	class materias{
		var $id = 0;
		var $nome = "";
		var $sigla = "";
		/*var $quantidade_manha=0;
		var $quantidade_tarde =0;
		var $quantidade_noite=0;
		var $quantidade_sabadoM =0;
		var $quantidade_sabadoT =0;
		var $quantidade_ead = 0;*/
		var $data_insercao="";
		var $data_alteracao = "";
	
	function Inserir(){
		$banco = new Sql;
		//$sql = 'Insert into materias(nome, sigla, quantidade_manha, quantidade_tarde, quantidade_noite, quantidade_sabadoM, quantidade_sabadoT, quantidade_ead, data_insercao) VALUES';
		//$sql .= '("'.$this->nome.'","'.$this->sigla.'", '.$this->quantidade_manha.','.$this->quantidade_tarde.', '.$this->quantidade_noite.', '.$this->quantidade_sabadoM.', '.$this->quantidade_sabadoT.', '.$this->quantidade_ead.', "'.$this->data_insercao.'")';
		$sql ='Insert into materias(nome, sigla, data_insercao) VALUES';
		$sql.= '("'.$this->nome.'","'.$this->sigla.'", "'.$this->data_insercao.'")'; 
		/*echo $sql;
		exit();*/
		$banco->Query($sql);
		$banco->Close();
	}
	function Alterar(){
		if($this->id){
			$banco = new Sql;
			$sql ='UPDATE materias SET';
			$sql .= ' nome ="'.$this->nome.'"';
			$sql .= ', sigla ="'.$this->sigla.'"';
			/*$sql .= ', quantidade_manha ='.$this->quantidade_manha;
			$sql .= ', quantidade_tarde ='.$this->quantidade_tarde;
			$sql .= ', quantidade_noite ='.$this->quantidade_noite;
			$sql .= ', quantidade_sabadoM ='.$this->quantidade_sabadoM;
			$sql .= ', quantidade_sabadoT ='.$this->quantidade_sabadoT;
			$sql .= ', quantidade_ead ='.$this->quantidade_ead;*/
			$sql .= ', data_alteracao ="'.$this->data_alteracao.'"';
			$sql .= ' WHERE id ='.$this->id;
			/*echo $sql;
			exit();*/
			$banco->Query($sql);
			$banco->Close();
		}	

	}
	function Excluir(){
		if($this->id){
			$banco = new Sql;
			$sql = 'DELETE FROM materias where id='.$this->id;
			$banco->Query($sql);
			$banco->Close();

		}
	}
	function Listar($where = '', $orderby = '', $limit =''){
		$banco = new Sql;
		$sql = 'SELECT *,DATE_FORMAT(data_insercao, "%d/%m/%Y") as dataformatada FROM materias';
		if($where)
			$sql .= ' WHERE '.$where;	
		if($orderby)
			$sql .= ' ORDER BY ' .$orderby;
		if($limit)
			$sql .=' LIMIT '.$limit;
		$banco->Query($sql);
		$ArrSql = $banco->SelecionaBanco($banco->getRecordSet());
		$banco->Close();
		if($ArrSql)
			return $ArrSql;
		else
			return false;
	}
	function SubtraiQuantidade($periodo='', $idMateria=0){
		$banco = new Sql;
		$sql = 'SELECT * FROM materias WHERE id='.$idMateria;
		$banco->Query($sql);
		$ArrSql = $banco->SelecionaBanco($banco->getRecordSet());
		if($ArrSql){
			if($periodo){
				$sql = '';
				$sql = 'UPDATE materias SET ';
				switch ($periodo) {
					case 'Manhã':
						$sql.='quantidade_manha = quantidade_manha - 1';
						break;

					case 'Tarde';
						$sql.='quantidade_tarde = quantidade_tarde - 1';
						break;
					
					case 'Noite';
						$sql.='quantidade_noite = quantidade_noite - 1';
						break;

					case 'Sabado Manhã';
						$sql.='quantidade_sabadoM = quantidade_sabadoM - 1';
						break;

					case 'Sabado Tarde';
						$sql.='quantidade_sabadoT = quantidade_sabadoT - 1';
						break;

					case 'EAD';
						$sql.='quantidade_ead= quantidade_ead - 1';
						break;
				}
				$sql .=' WHERE id='.$idMateria;
				$banco->Query($sql);
			}
		}
	}
	function AdicionaQuantidade($periodo='', $idMateria=0){
		$banco = new Sql;
		$sql = 'SELECT * FROM materias WHERE id='.$idMateria;
		$banco->Query($sql);
		$ArrSql = $banco->SelecionaBanco($banco->getRecordSet());
		if($ArrSql){
			if($periodo){
				$sql = '';
				$sql = 'UPDATE materias SET ';
				switch ($periodo) {
					case 'Manhã':
						$sql.='quantidade_manha = quantidade_manha + 1';
						break;

					case 'Tarde';
						$sql.='quantidade_tarde = quantidade_tarde + 1';
						break;
					
					case 'Noite';
						$sql.='quantidade_noite = quantidade_noite + 1';
						break;

					case 'Sabado Manhã';
						$sql.='quantidade_sabadoM = quantidade_sabadoM + 1';
						break;

					case 'Sabado Tarde';
						$sql.='quantidade_sabadoT = quantidade_sabadoT + 1';
						break;

					case 'EAD';
						$sql.='quantidade_ead= quantidade_ead + 1';
						break;
				}
				$sql .=' WHERE id='.$idMateria;
				$banco->Query($sql);
			}
		}
	}
}

?>