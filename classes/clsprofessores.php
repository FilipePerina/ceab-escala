<?php 
	require_once('mysql.php');
	require_once('clsmaterias.php');
	class professores{
		var $id = 0;
		var $nome ="";
		var $dias_manha ="";
		var $dias_tarde ="";
		var $dias_noite ="";
		var $dias_sabadoM ="";
		var $dias_sabadoT ="";
		var $dias_ead ="";
		var $email ="";
		var $senha ="";
		var $data_insercao ="";
		var $data_alteracao ="";
		
	function Inserir(){
		$banco = new Sql;
		$sql = 'INSERT INTO professores(nome, dias_manha, dias_tarde, dias_noite, dias_sabadoM, dias_sabadoT, dias_ead, email, senha, data_insercao) VALUES';
		$sql .= '("'.$this->nome.'","'.$this->dias_manha.'","'.$this->dias_tarde.'", "'.$this->dias_noite.'", "'.$this->dias_sabadoM.'", "'.$this->dias_sabadoT.'","'.$this->dias_ead.'", "'.$this->email.'", "' . $this->senha . '","' .$this->data_insercao.'")';
		
		if($this->materias){
			$banco->Query($sql);
			$sqlUltimoRegistro ='SELECT MAX(id) as UltimoRergistro FROM professores';
			$banco->Query($sqlUltimoRegistro);
			$ArrUltimo = $banco->SelecionaBanco($banco->getRecordSet());
			$ObjMaterias = new materias;
			for($i=0; $i<count($this->materias); $i++){
				$sqlMaterias = 'INSERT INTO professores_rel_materias(id_professor, id_materia) VALUES';
				$sqlMaterias .= ' ('.$ArrUltimo[0]['UltimoRergistro'].', '.$this->materias[$i].')';
				$banco->Query($sqlMaterias);
				$sqlMaterias = '';
			}
			/*echo $sql;
			exit();*/
			$banco->Close();
		}
	}
	function Alterar(){
		if($this->id){
			$banco = new Sql;
			$sqlRelacao = 'DELETE FROM professores_rel_materias WHERE id_professor='.$this->id;
			$banco->Query($sqlRelacao);

			$sql ='UPDATE professores SET';
			$sql .= ' nome ="'.$this->nome.'"';
			$sql .= ', dias_manha ="'.$this->dias_manha.'"';
			$sql .= ', dias_tarde ="'.$this->dias_tarde.'"';
			$sql .= ', dias_noite ="'.$this->dias_noite.'"';
			$sql .= ', dias_sabadoM ="'.$this->dias_sabadoM.'"';
			$sql .= ', dias_sabadoT ="'.$this->dias_sabadoT.'"';
			$sql .= ', dias_ead ="'.$this->dias_ead.'"';
			$sql .= ', email ="'.$this->email.'"';
			$sql .= ', senha ="'.$this->senha.'"';
			$sql .= ', data_alteracao ="'.$this->data_alteracao.'"';
			$sql .= ' WHERE id ='.$this->id;
			/*echo $sql;
			exit();*/
			$banco->Query($sql);
			for($i=0; $i<count($this->materias); $i++){
				$sqlMaterias = 'INSERT INTO professores_rel_materias(id_professor, id_materia) VALUES';
				$sqlMaterias .= ' ('.$this->id.', '.$this->materias[$i].')';
				$banco->Query($sqlMaterias);
				$sqlMaterias = '';
			}
			$banco->Close();
		}	

	}
	function Excluir(){
		if($this->id){
			$banco = new Sql;
			$sql = 'DELETE FROM professores where id='.$this->id;
			$banco->Query($sql);
			$sqlRelacao = 'DELETE FROM professores_rel_materias WHERE id_professor='.$this->id;
			$banco->Query($sqlRelacao);
			$banco->Close();
		}
	}
	function Listar($where = '', $orderby = '', $limit =''){
		$banco = new Sql;
		$sql = 'SELECT *,DATE_FORMAT(data_insercao, "%d/%m/%Y") as dataformatada FROM professores';
		if($where)
			$sql .= ' WHERE '.$where;	
		if($orderby)
			$sql .= ' ORDER BY ' .$orderby;
		if($limit)
			$sql .=' LIMIT '.$limit;
		$banco->Query($sql);
		$ArrSql = $banco->SelecionaBanco($banco->getRecordSet());
		$banco->Close();
		if($ArrSql)
			return $ArrSql;
		else
			return false;
	}
	function ListarMateriasAssociadas($periodo=''){
		if($this->id){
			$banco = new Sql;
			$sql = 'SELECT C.sigla as nomeMateria, C.id as idMateria FROM professores_rel_materias as A, professores as B, materias as C';
			$sql .= ' WHERE A.id_professor ='.$this->id.' and A.id_materia = C.id ';
			/*switch ($periodo) {
				case 'Manhã':
					$sql.=' AND C.quantidade_manha > 0';
					break;

				case 'Tarde';
					$sql.=' AND C.quantidade_tarde > 0';
					break;
				
				case 'Noite';
					$sql.=' AND C.quantidade_noite > 0';
					break;

				case 'Sabado Manhã';
					$sql.=' AND C.quantidade_sabadoM > 0';
					break;

				case 'Sabado Tarde';
					$sql.=' AND C.quantidade_sabadoT > 0';
					break;

				case 'EAD';
					$sql.=' AND C.quantidade_ead > 0';
					break;
			}*/

			$sql.= ' GROUP BY nomeMateria';
			/*echo $sql;
			exit();*/
			$banco->Query($sql);
			$ArrSql = $banco->SelecionaBanco($banco->getRecordSet());
			$banco->Close();
			if($ArrSql)
				return $ArrSql;
			else
				return false;
		}
	}
	function ZerarProfessores(){
		$banco = new Sql;
		$sql = 'UPDATE professores SET';
		$sql .= '  dias_manha = ""';
		$sql .= ', dias_tarde = ""';
		$sql .= ', dias_noite = ""';
		$sql .= ', dias_sabadoM = ""';
		$sql .= ', dias_sabadoT = ""';
		$sql .= ', dias_ead = ""';
		$banco->Query($sql);
		$banco->Close();
	}
}

?>