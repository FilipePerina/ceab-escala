<?php 
	session_start();
	if ($_SESSION['logado'] != 'S') {
		header('Location: index.php');
	}
require_once('include/functions.php');
require_once('classes/clsmaterias.php');
require_once('classes/clsprofessores.php');
require_once('classes/clsrelprofessormateria.php');
$ObjProfessores = new professores;

$id = RecebeParametro('id');
if($id){
	$acao ='alterar&id='.$id;
	$ArrProfessores = $ObjProfessores->Listar('id='.$id,'','');
	if($ArrProfessores){
		$nome = $ArrProfessores[0]['nome'];
		$dias_manha = $ArrProfessores[0]['dias_manha'];
		$dias_tarde = $ArrProfessores[0]['dias_tarde'];
		$dias_noite = $ArrProfessores[0]['dias_noite'];
		$dias_sabadoM = $ArrProfessores[0]['dias_sabadoM'];
		$dias_sabadoT = $ArrProfessores[0]['dias_sabadoT'];
		$dias_ead = $ArrProfessores[0]['dias_ead'];
		$email = $ArrProfessores[0]['email'];
		$password = $ArrProfessores[0]['senha'];
	}
}else
	$acao = 'incluir';

$str =RecebeParametro('str');
if($str=='incluir'){
	$ObjProfessores->nome =  strtoupper(RecebeParametro('nome'));
	$ObjProfessores->dias_manha = RecebeParametro('dias_manha');
	$ObjProfessores->dias_tarde = RecebeParametro('dias_tarde');
	$ObjProfessores->dias_noite = RecebeParametro('dias_noite');
	$ObjProfessores->dias_sabadoM = RecebeParametro('dias_sabadoM');
	$ObjProfessores->dias_sabadoT = RecebeParametro('dias_sabadoT');
	$ObjProfessores->dias_ead = RecebeParametro('dias_ead');
	$ObjProfessores->email = RecebeParametro('email');
	$ObjProfessores->senha = RecebeParametro('senha');
	$ObjProfessores->materias = $_POST['materias'];
	$ObjProfessores->data_insercao = date('Y-m-d');
	$ObjProfessores->Inserir();
	echo'<script>alert("Dados Salvos !"); location.href="materias.php";</script> ';
}else if($str=='alterar'){
	$ObjProfessores->nome = strtoupper(RecebeParametro('nome'));
	$ObjProfessores->dias_manha = RecebeParametro('dias_manha');
	$ObjProfessores->dias_tarde = RecebeParametro('dias_tarde');
	$ObjProfessores->dias_noite = RecebeParametro('dias_noite');
	$ObjProfessores->dias_sabadoM = RecebeParametro('dias_sabadoM');
	$ObjProfessores->dias_sabadoT = RecebeParametro('dias_sabadoT');
	$ObjProfessores->dias_ead = RecebeParametro('dias_ead');
	$ObjProfessores->email = RecebeParametro('email');
	$ObjProfessores->senha = RecebeParametro('senha');
	$ObjProfessores->materias = $_POST['materias'];
	$ObjProfessores->data_alteracao = date('Y-m-d');
	$ObjProfessores->id = $id;
	$ObjProfessores->Alterar();
	echo'<script>alert("Dados alterados com Sucesso !"); location.href="lista-professores.php";</script> ';
}
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<title>Sistema de Escala - CEAB -Brasil</title>
	<link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="css/css.css" />
</head>
<body>
<?php require_once('topo.php') ?>
	<section class="alinha960">
		<h1><img src="images/setas.png" alt=""> Cadastrar Professor</h1>
	 <form action="professores.php?str=<?php echo $acao; ?>" method="post">
		 <table width="100%" name="tabusu" class="tabelaconteudo tabela-professores">
	 		<tbody>
	 			<tr>
	 				<td width="200">Nome do Professor:</td>
	 				<?php  InputText('nome','','','','',$nome);?>
	 			</tr>
	 			<tr>
	 				<td>Dias disponíveis Manhã:</td>
	 				<?php  InputText('dias_manha','','','','',$dias_manha);?>
	 			</tr>
	 			<tr>
		 			<td></td>
		 			<td><div id="date_dias_manha"></div></td>
	 			</tr>
	 			<tr>
	 				<td>Dias Tarde:</td>
	 				<?php  InputText('dias_tarde','','','','',$dias_tarde);?>
	 			</tr>
	 			<tr>
		 			<td></td>
		 			<td><div id="date_dias_tarde"></div></td>
	 			</tr>
	 			<tr>
	 				<td>Dias disponíveis Noite:</td>
	 				<?php  InputText('dias_noite','','','','',$dias_noite);?>
	 			</tr>
	 			<tr>
		 			<td></td>
		 			<td><div id="date_dias_noite"></div></td>
	 			</tr>
	 			<!--<tr>
	 				<td>Dias disponíveis Sabado Manhã:</td>
	 				<?php  InputText('dias_sabadoM','','','','',$dias_sabadoM);?>
	 			</tr>
	 			<tr>
	 				<td>Dias disponíveis Sabado Tarde:</td>
	 				<?php  InputText('dias_sabadoT','','','','',$dias_sabadoT);?>
	 			</tr>-->
	 			<tr>
	 				<td>Dias disponíveis EAD:</td>
	 				<?php  InputText('dias_ead','','','','',$dias_ead);?>
	 			</tr>
	 			<tr>
		 			<td></td>
		 			<td><div id="date_dias_ead"></div></td>
	 			</tr>
	 			<tr>	
	 				<td>Matérias:</td>
	 				<td class="todas-materias">
	 					<?php 
	 						$ObjMaterias = new materias;
	 						$ArrMaterias = $ObjMaterias->Listar('','nome ASC','');
	 						if(is_array($ArrMaterias)){
	 							$ObjRelacao = new relprofessormateria;
	 							foreach ($ArrMaterias as $row) {
	 								
	 					 ?>
	 					 <div class="materias-holder">
	 					<input type="checkbox" name="materias[]" id="" value="<?php echo $row['id']?>" <?php 
	 						if($id){
	 							$ObjRelacao->id_professor=$id;
	 							$ObjRelacao->id_materia = $row['id'];
	 							$ArrRelacao = $ObjRelacao->ListarMateriasAssociadas();
	 							if($ArrRelacao){
	 								foreach($ArrRelacao as $rowRelacao){
	 									if($rowRelacao['IdMateria'] == $row['id'])
	 										echo "checked";
		 						}
		 					}
		 				}	
	 					 ?>><?php echo $row['nome']; ?></div>
	 					<?php 
	 						}
	 					}
	 					 ?>
	 				</td>
	 			</tr>
	 			<tr>
	 				<td>Email:</td>
	 				<?php  InputText('email','','','','',$email);?>
	 			</tr>
	 			<tr>
	 				<td>Senha:</td>
	 				<?php  InputText('senha','','','','',$password);?>
	 			</tr>
	 			<tr>
	 					<td colspan="2" align="center">
	 						<input type="submit" value="Gravar">
	 					</td>
	 				</tr>	
	 		</tbody>
		 	</table>
	 	</form>
	 	
	</section>

	<?php require_once('rodape.php'); ?>
</body>
</html>