<?php
	session_start();
	if ($_SESSION['logado'] != 'S') {
		header('Location: index.php');
	}
require_once('classes/clsprofessores.php');
require_once('include/functions.php');
$ObjProfessor = new professores;
$tipoRelatorio = RecebeParametro('relatorio');

$reportType = $_POST["report-escala"];

if($reportType == "comissario"){
	$escala = "comissario";
}else{
	$escala = "checkin";
}

if(!$tipoRelatorio)
	echo '<script>alert("Nenhum relatório selecionado !); location.href="relatorios.php"; </script>';
elseif($tipoRelatorio == 'prof')
	$relatorio = 'Relatorio Individual de professores';
else
	$relatorio = 'Relatório Quantidade de aula por professor';

?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<title>Sistema de Escala - CEAB -Brasil</title>
	<link rel="stylesheet" href="css/css.css" />
	<style>
	.geraRelatorio {
  -webkit-border-radius: 5;
  -moz-border-radius: 5;
  border-radius: 5px;
  font-family: Arial;
  color: #ffffff !important;
  font-size: 12px;
  background: #6e34d9;
  padding: 5px 10px 5px 10px;
  text-decoration: none;
}

.geraRelatorio:hover {
  background: #8a6aab;
  text-decoration: none;
}
.geraRelatorio a{color: #FFF !important;}
	</style>
</head>
<body>
<?php require_once('topo.php') ?>
	<section class="alinha960">
		<h1><img src="images/setas.png" alt=""> <?php echo $relatorio; ?></h1>
	 <form action="relatorios.php" method="post">
		 <table width="100%" name="tabusu" class="tabelaconteudo">
	 		<thead>
	 			<tr>
	 				<td>Nome do Professor</td>
	 				<td>Ação</td>
	 			</tr>
	 		</thead>
	 		<tbody>
	 			<?php
	 				$ArrProfessor = $ObjProfessor->Listar('','nome asc','');
	 				if(is_array($ArrProfessor)){
	 					foreach($ArrProfessor as $row){
	 			?>
	 			<tr>
	 				<td><?php echo $row['nome']; ?></td>
	 				<td>
	 					<?php
	 						if($tipoRelatorio == 'prof'){
	 					 ?>
	 					<a href="imprimir-relatorio-indivdual.php?id=<?php echo $row['id']; ?>&escala=<?php echo $escala; ?>" target="_blank" class="geraRelatorio">Gerar relatório</a>
	 					<?php
	 						}else{
	 					 ?>
	 					 <a href="imprimir-relatorio-quantidade.php?id=<?php echo $row['id']; ?>&escala=<?php echo $escala; ?>" target="_blank" class="geraRelatorio">Gerar relatório</a>
	 					 <?php
	 					 	}
	 					  ?>
	 				</td>
	 			</tr>
	 			<?php
	 					}
	 				}
	 			 ?>
	 		</tbody>
		 	</table>
	 	</form>
	</section>
	<?php require_once('rodape.php'); ?>
</body>
</html>
