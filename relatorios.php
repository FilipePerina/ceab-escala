<?php 
	session_start();
	if ($_SESSION['logado'] != 'S') {
		header('Location: index.php');
	}
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<title>Sistema de Escala - CEAB -Brasil</title>
	<link rel="stylesheet" href="css/css.css" />
	<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body>
<?php require_once('topo.php') ?>
	<section class="alinha960">
		<h1><img src="images/setas.png" alt=""> Selecionar Relatório</h1>
		
	 <form action="relatorio-professor.php" method="post">
		 <table width="100%" name="tabusu" class="tabelaconteudo">
	 		<thead>
	 			<tr>
	 				<td>Nome do Professor</td>
	 				<td>Ação</td>
	 			</tr>
	 		</thead>
	 		<tbody>
				<tr>
					<td>Escala</td>
					<td>
						<input type="radio" name="report-escala" value="comissario" checked> Comissário<br/>
						<input type="radio" name="report-escala" value="checkin"> Check-in<br/>
					</td>
				</tr>
	 			<tr>
	 				<td>Relatório individual de cada professor:</td>
	 				<td><input type="radio" name="relatorio" id="" value="prof"></td>
	 			</tr>
	 			<tr>
	 				<td>Relatório Quantidade de aula por professor</td>
	 				<td><input type="radio" name="relatorio" id="" value="quantidade"></td>
	 			</tr>
	 			<tr>
 					<td colspan="2" align="center">
 						<input type="submit" value="Próximo >>">
 					</td>
 				</tr>	
	 		</tbody>
		 	</table>
	 	</form>
		
	</section>

	<?php require_once('rodape.php'); ?>
</body>
</html>
