<?php
	session_start();
	if ($_SESSION['logado'] != 'S') {
		header('Location: index.php');
	}
require_once('include/functions.php');
require_once('classes/clsturmas.php');
$ObjTurmas = new turmas;
$id = RecebeParametro('id');
if($id){
	$acao ='alterar&id='.$id;
	$ArrTurmas = $ObjTurmas->Listar('id='.$id,'','');
	if($ArrTurmas){
		$nome = $ArrTurmas[0]['nome'];
		$periodo = $ArrTurmas[0]['periodo'];
		$data_inicial = $ArrTurmas[0]['datainicial'];
		$data_final = $ArrTurmas[0]['datafinal'];
		$tipo = $ArrTurmas[0]['tipo'];
		$corturma = $ArrTurmas[0]['corturma'];
		$sala = $ArrTurmas[0]['sala'];
	}
}else
	$acao = 'incluir';

$str =RecebeParametro('str');
if($str=='incluir'){
	$ObjTurmas->nome = RecebeParametro('nome');
	$ObjTurmas->periodo = RecebeParametro('periodo');
	$ObjTurmas->data_inicial = dateFormatMysql(RecebeParametro('datainicial'));
	$ObjTurmas->data_final = dateFormatMysql(RecebeParametro('datafinal'));
	$ObjTurmas->tipo = RecebeParametro('tipocurso');
	$ObjTurmas->corturma = RecebeParametro('corturma');
	$ObjTurmas->sala = RecebeParametro('sala');
	$ObjTurmas->data_insercao = date('Y-m-d');
	$ObjTurmas->Inserir();
	echo'<script>alert("Dados Salvos !"); location.href="turmas.php";</script> ';
}else if($str=='alterar'){
	$ObjTurmas->nome = RecebeParametro('nome');
	$ObjTurmas->periodo = RecebeParametro('periodo');
	$ObjTurmas->data_inicial = dateFormatMysql(RecebeParametro('datainicial'));
	$ObjTurmas->data_final = dateFormatMysql(RecebeParametro('datafinal'));
	$ObjTurmas->tipo = RecebeParametro('tipocurso');
	$ObjTurmas->corturma = RecebeParametro('corturma');
	$ObjTurmas->sala = RecebeParametro('sala');
	$ObjTurmas->data_alteracao = date('Y-m-d');
	$ObjTurmas->id = $id;
	$ObjTurmas->Alterar();
	echo'<script>alert("Dados alterados com Sucesso !"); location.href="lista-turmas.php";</script> ';
}

?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<title>Sistema de Escala - CEAB -Brasil</title>
	<link rel="stylesheet" href="css/css.css" />
 	<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
	<script src="js/colpick.js" type="text/javascript"></script>
	<link rel="stylesheet" href="css/colpick.css" type="text/css"/>
	<style>
#picker {
	margin:0;
	padding:0;
	border:0;
	width:70px;
	height:20px;
	border-right:20px solid green;
	line-height:20px;
}
	</style>
</head>
<body>
	
	<script>
		jQuery(document).ready(function($){
			$( "#datepicker" ).datepicker({ dateFormat: "dd/mm/yy" });
			$( "#datepicker2" ).datepicker({ dateFormat: "dd/mm/yy" });
		});
	</script>
	
<?php require_once('topo.php') ?>
	<section class="alinha960">
		<h1><img src="images/setas.png" alt=""> Cadastrar Turmas</h1>
	 <form action="turmas.php?str=<?php echo $acao; ?>" method="post">
		 <table width="100%" name="tabusu" class="tabelaconteudo">
	 		<tbody>
	 			<tr>
	 				<td>Nome da Turma:</td>
	 				<?php  InputText('nome','','','','',$nome);?>
	 			</tr>
	 			<tr>
	 				<td>Período da Turma:</td>
	 				<td>
	 					<select name="periodo" id="">
	 						<option value="" selected>Selecione</option>
	 						<option value="Manha" <?php if($periodo =='Manha') echo 'selected'; ?>>Manhã</option>
	 						<option value="Tarde" <?php if($periodo =='Tarde') echo 'selected'; ?>>Tarde</option>
	 						<option value="Noite" <?php if($periodo =='Noite') echo 'selected'; ?>>Noite</option>
	 						<option value="Sabado Manha" <?php if($periodo =='Sabado Manha') echo 'selected'; ?>>Sábado Manhã</option>
	 						<option value="Sabado Tarde" <?php if($periodo =='Sabado Tarde') echo 'selected'; ?>>Sábado Tarde</option>
	 						<option value="EAD" <?php if($periodo =='EAD') echo 'selected'; ?>>EAD</option>
	 					</select>
	 				</td>
	 			</tr>
	 			<tr>
	 				<td>Data ínicio:</td>
	 				<td><input type="text" name="datainicial" id="datepicker" value="<?php echo $data_inicial?>">Formato(dd/mm/aaaa)</td>
	 			</tr>
	 			<tr>
	 				<td>Data fim:</td>
	 				<td><input type="text" name="datafinal" id="datepicker2" value="<?php echo $data_final?>" >Formato(dd/mm/aaaa)</td>
	 			</tr>
	 			<tr>
	 				<td>Tipo Curso:</td>
	 				<td>
	 					<?php
	 						if($tipo == 1){
	 							InputRadio('tipocurso', '', '', 'checked','nao', '1'); echo'Comissário de Bordo <br>';
	 							InputRadio('tipocurso', '', '', '','nao', '2'); echo'Check-in';
	 						}elseif($tipo==2){
	 							InputRadio('tipocurso', '', '', '','nao', '1'); echo'Comissário de Bordo <br>';
	 							InputRadio('tipocurso', '', '', 'checked','nao', '2'); echo'Check-in';
	 						}else{
	 							InputRadio('tipocurso', '', '', 'checked','nao', '1'); echo'Comissário de Bordo <br>';
	 							InputRadio('tipocurso', '', '', '','nao', '2'); echo'Check-in';
	 						}
	 					?>
	 				</td>
	 			</tr>
	 			<tr>
	 				<td>Cor da Turma:</td>
	 				<td>
	 					#<input type="text" name="corturma" id="picker" style="border-color: <?php echo "#" . $corturma; ?>" value="<?php echo $corturma;?>">
	 				</td>
	 			</tr>
	 			<tr>
	 				<td>Sala: <strong>Apenas Números</strong></td>
	 				<?php InputText('sala','','','','',$sala); ?>
	 			</tr>
	 			<tr>
	 					<td colspan="2" align="center">
	 						<input type="submit" value="Gravar">
	 					</td>
	 				</tr>
	 		</tbody>
		 	</table>
	 	</form>

	</section>

	<?php require_once('rodape.php'); ?>

<script>
	$('#picker').colpick({
		layout:'hex',
		submit:0,
		colorScheme:'dark',
		onChange:function(hsb,hex,rgb,el,bySetColor) {
			$(el).css('border-color','#'+hex);
			// Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
			if(!bySetColor) $(el).val(hex);
		}
	}).keyup(function(){
		$(this).colpickSetColor(this.value);
	});
	</script>
	</body>
</html>
