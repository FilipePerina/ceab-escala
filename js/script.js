jQuery(document).ready(function($){
	
	function setPeriod(period){
		
		$('.period').removeClass('enabled');
		$('.period').removeClass('disabled');
		
		if(period == 'Manha'){
			$('.period.morning').addClass('enabled');
			$('.period.afternoon').addClass('disabled');
			$('.period.night').addClass('disabled');
			$('.period.ead').addClass('disabled');
			
			console.log('Period set to '+period);
		}else if(period == "Tarde"){
			$('.period.morning').addClass('disabled');
			$('.period.afternoon').addClass('enabled');
			$('.period.night').addClass('disabled');
			$('.period.ead').addClass('disabled');
			
			console.log('Period set to '+period);
		}else if(period == "Noite"){
			$('.period.morning').addClass('disabled');
			$('.period.afternoon').addClass('disabled');
			$('.period.night').addClass('enabled');
			$('.period.ead').addClass('disabled');
			
			console.log('Period set to '+period);
		}else if(period == "EAD"){
			$('.period.morning').addClass('disabled');
			$('.period.afternoon').addClass('disabled');
			$('.period.night').addClass('disabled');
			$('.period.ead').addClass('enabled');
			
			console.log('Period set to '+period);
		}
		
		
		
	}
	
	function loadTeachers(){
		$.post('actions.php', { act: 'list-teachers' }, function(data){
			var obj = jQuery.parseJSON(data);
			
			//console.log(data);
			
			$('#teacher-list').html("");
			
			$.each(obj, function(idx, obj){
				var id 			= obj.professores.id;
				var name 		= obj.professores.nome;
				var morning 	= obj.professores.dias_manha;
				var afternoon 	= obj.professores.dias_tarde;
				var night		= obj.professores.dias_noite;
				var ead			= obj.professores.dias_ead;
				
				var result		= '<div class="teacher" data-morning="'+morning+'" data-afternoon="'+afternoon+'" data-night="'+night+'" data-ead="'+ead+'">';
				result			+=		'<img src="http://placehold.it/100x100"><br/>';
				result			+=		'<span>'+name+'</span>';
				result			+=	'</div>';
				
				$('#teacher-list').append(result);
				
			});
			
		});
	}
	
	$('#materia, #turma').select2();
	
	$('#imprimir-comissario, #imprimir-checkin, #imprimir-escala').click(function(){
		$('.printThis').printElement({
            overrideElementCSS:[
			'css-print.css',
			{ href:'css/css-print.css',media:'print'}]
        });
	});
	
	function fixDiv() {
	    var $teachers = $('#teacher-list');
	    if ($(window).scrollTop() > 520){
	      $teachers.addClass('teacher-list-fixed');
	    }else{
	      $teachers.removeClass('teacher-list-fixed');
	    }
	  }
	  $(window).scroll(fixDiv);
	  fixDiv();
	
	if($('body').hasClass('login')){
	
		$('#login-form').submit(function(e){
			e.preventDefault();
			var values = $(this).serialize();
						
			$.post('actions.php?act=login', values, function(data){
				var obj = $.parseJSON(data);
				
				if(obj.status == 1){
					if(obj.user == 'admin'){
						window.location.href = "home.php";
					}else{
						window.location.href = "professor-dash.php";
					}
					
				}else if(obj.status == '0'){
					$('.login-message').text(obj.message).slideDown();
					
					setTimeout(function(){
						$('.login-message').slideUp();
					}, 3000);
					
				}
				
			});
			
		});
			
	} // Body Has Class "login"
	
	$('td.teacher-status a').click(function(){
		var id = $(this).data('id');
		var status = $(this).data('status');
		
		if(status == 1){
			$(this).removeClass('red');
			$(this).addClass('green');
			$(this).data('status','0');
		}else{
			$(this).removeClass('green');
			$(this).addClass('red');
			$(this).data('status','1');		
		}
		
		$.post('actions.php?act=teacher-status', {id:id,status:status}, function(data){
			
		});
		
	});
	
	$('.teacher-dash .period').click(function(){
		var teacher_id = $(this).data('teacher-id');
		var day = $(this).data('day');
		var period = $(this).data('period');
				 
		$.post('actions.php?act=day-select', {teacher_id:teacher_id, day:day, period:period}, function(data){
			if(data){
				alert(data);
			}
		});
		
		if($(this).hasClass('day_selected')){
			$(this).removeClass('day_selected');
		}else{
			$(this).addClass('day_selected');
		}
	});
	
	$('#teacher_id, #select_turma_id').change(function(){
		$('#teacher_form').submit();
	});
	
	$('.change_photo').click(function(e){
		e.preventDefault();
		var id = $(this).data('id');
		$('form[data-id="'+id+'"] .image_input').trigger('click');
	});
	
	$('.image_input').change(function(e){
		$(this).parent().submit();
	});
	
	if($('body').hasClass('create-schedule')){
		
		var schedule = $('body').data('schedule');
		var SelectedTurma = false;
		var SelectedTurmaId = false;
		var SelectedMateria = false;
		
		$.post('actions.php', { act: 'list-classes', schedule:schedule}, function(data){
			var obj = jQuery.parseJSON(data);
			
			$.each(obj, function(idx, obj){
				var id = obj.turmas.id;
				var name = obj.turmas.nome;
				var period = obj.turmas.periodo;
				var sala = obj.turmas.sala;
				
				var title = name + " - " + period + " - " + " SALA " + sala;
				
				$('#classes').append("<option value='"+id+":"+period+"'>"+ title +"</option>");
				
			});
			
		});
		
		$('#classes').on('change', function(){
			var val = $(this).val();
			val = val.split(":");
			
			var id = val[0];
			var period = val[1];

			if(period && id){
				loadTeachers();
				setPeriod(period);
			}

			
		});
			
	}
	
	
	// SISTEMA
	
	$('#turma').change(function(e){
		var id = $(this).val();

		if(!id){ 
			SelectedTurma = false; 
			SelectedTurmaId = false;
			$('.period').removeClass('disabled');
			
			$('#materia-row').addClass('hide');
			$('.teacher-list .teacher').addClass('hide');
			
			return false; 
		}
		
		$.post('actions.php', { act:"get-class", id:id }, function(data){
			var obj = jQuery.parseJSON(data);
			console.log(obj);
			SelectedTurma = obj[0].turmas.periodo;
			SelectedTurmaId = id;
			
			$('.period').addClass('disabled');
			$('.period[data-period="'+ SelectedTurma +'"]').removeClass('disabled');
			$('.teacher-list .teacher').data('color', "#" + obj[0].turmas.corturma);
						
			$('#materia-row').removeClass('hide');
		});
		
		console.log(id);
				
	});
	
	$('#turma').trigger('change');
	
	$('#materia').change(function(e){
		var id = $(this).val();
		
		if(!id){ SelectedMateria = false; return false; }
		
		SelectedMateria = id;
		$('.teacher-list .teacher').addClass('hide');
		
		$.post('actions.php', { act:"get-teachers", id:id }, function(data){
			var obj = jQuery.parseJSON(data);
						
			$.each(obj, function(index, data){
				data = data.professores_rel_materias;
				
				$('.teacher-list .teacher[data-id="'+ data.id_professor +'"]').removeClass('hide');
			});
		});
	});
    
    var oldObj = '';
    
    function startTeacher(){
	    
	    oldObj = $(this);
	    
	    var morning = $(this).data('morning');
		var afternoon = $(this).data('afternoon');
		var night = $(this).data('night');
		var ead = $(this).data('ead');				
					
		morning = morning.split(',');
		afternoon = afternoon.split(',');
		night = night.split(',');
		ead = ead.split(',');
		
				
		$('.period').addClass('disabled');
		$('.period').removeClass('drop');
							
		$.each(morning, function(index, day){
			if(!SelectedTurma){
				$('.day-item[data-date="'+ day +'"] .period.morning').removeClass('disabled');
				$('.day-item[data-date="'+ day +'"] .period.morning').addClass('drop');	
			}else{
				$('.day-item[data-date="'+ day +'"] .period[data-period="'+ SelectedTurma +'"].morning').addClass('drop');
				$('.day-item[data-date="'+ day +'"] .period[data-period="'+ SelectedTurma +'"].morning').removeClass('disabled');
			}
		});
		
		$.each(afternoon, function(index, day){
			if(!SelectedTurma){
				$('.day-item[data-date="'+ day +'"] .period.afternoon').removeClass('disabled');
				$('.day-item[data-date="'+ day +'"] .period.afternoon').addClass('drop');
			}else{
				$('.day-item[data-date="'+ day +'"] .period[data-period="'+ SelectedTurma +'"].afternoon').addClass('drop');
				$('.day-item[data-date="'+ day +'"] .period[data-period="'+ SelectedTurma +'"].afternoon').removeClass('disabled');
			}
		});
		
		$.each(night, function(index, day){
			if(!SelectedTurma){
				$('.day-item[data-date="'+ day +'"] .period.night').removeClass('disabled');
				$('.day-item[data-date="'+ day +'"] .period.night').addClass('drop');
			}else{
				$('.day-item[data-date="'+ day +'"] .period[data-period="'+ SelectedTurma +'"].night').addClass('drop');
				$('.day-item[data-date="'+ day +'"] .period[data-period="'+ SelectedTurma +'"].night').removeClass('disabled');
			}
		});
		
		$.each(ead, function(index, day){
			if(!SelectedTurma){
				$('.day-item[data-date="'+ day +'"] .period.ead').removeClass('disabled');
				$('.day-item[data-date="'+ day +'"] .period.ead').addClass('drop');
			}else{
				$('.day-item[data-date="'+ day +'"] .period[data-period="'+ SelectedTurma +'"].ead').addClass('drop');
				$('.day-item[data-date="'+ day +'"] .period[data-period="'+ SelectedTurma +'"].ead').removeClass('disabled');
			}
		});
    }
    
	
    
    setInterval(function(){
	    $('.teacher-list .teacher').draggable({
			helper: "clone",
			revert: false,
			start: startTeacher,
			stop: function(ev, ui){
				if(!SelectedTurma){
					$('.period').removeClass('disabled');
				}else{					
					$('.period').addClass('disabled');
					$('.period[data-period="'+ SelectedTurma +'"]').removeClass('disabled');
				}
				
			},
	    });
	    
	    $( ".drop" ).droppable({
	    	accept: ".teacher",
	    	hoverClass: "highlighted",
	    	drop: function(ev, ui){
		    	if($(this).hasClass('drop')){
			    	
			    	var checkTeacher = false;
			    	
			    	$(this).find('.teacher').each(function(){
				    	if($(this).data('id') == $(ui.draggable).data('id')){
					    	checkTeacher = true;
				    	}
			    	});
			    	
			    	if(checkTeacher){
				    	return false;
			    	}
			    	
			    	var id = $(ui.draggable).data('id');
			    	var morning = $(ui.draggable).data('morning');
			    	var afternoon = $(ui.draggable).data('afternoon');
			    	var night = $(ui.draggable).data('night');
			    	var turmaColor = $(ui.draggable).data('color');			    	
			    	
			    	//var before = '<div class="teacher" style="background: '+ turmaColor +'" data-color="'+turmaColor+'" data-id="'+id+'" data-morning="'+morning+'" data-afternoon="'+afternoon+'" data-night="'+night+'">';
			    	
			    	var placeToAppend = $(this);
			    	
			    	//$(this).append(before + $(ui.draggable).html() + "</div>");
			    	
			    	// SAVE DATA TO ESCALA_CHECKIN
			    	if(SelectedTurmaId){
				    	
				    	if(SelectedMateria){
					    	
					    	var aula_date = $(this).data('aula-date'); 
							var period = $(this).data('period');
							var escala = $('#escala').val();
					    	
					    	$.post("actions.php", { act:'add-escala', escala:escala, id_turma:SelectedTurmaId, id_professor:id, id_materia:SelectedMateria, data_aula:aula_date, periodo:period }, function(data){
						    	placeToAppend.append(data);
					    	});
					    	
					    	
				    	}else{ alert(' Matéria Não Selecionada!'); }
				    	
			    	}else{ alert('Turma Não Selecionada!'); }
			    	
			    	
			    	if($(oldObj).parent().hasClass('teacher-list')){
				    	
			    	}else{
				    	$(oldObj).remove();
			    	}
			    	
		    	}
	    	}
	    });
	    
	   
	    
	    $('[data-toggle="popover"]').click(function(e){
	    	e.preventDefault();
    	});
		$('[data-toggle="popover"]').popover({
			 trigger: "hover" 
		});
	    
	    
    }, 500);
    
    
	 $('.delete-escala').live('click', function(e){
		e.preventDefault();
	    var id = $(this).data('escala-id');
	    var escala = $('#escala').val();
	    
	    $(this).parent().parent().remove();
	    $.post("actions.php", { act:"delete-escala", escala:escala, id:id}, function(data){
		    
	    });
    });
    
    
    $('#select_month').change(function(){
	   var month = $(this).val();
	   var year = $('#select_year').val();
	   var escala = $(this).data('escala');
	   var teacher = $('#teacher_id').val();
	   var turma = $('#select_turma_id').val();
	   
	   window.location.href = "?month="+month+"&year="+year + (escala ? "&escala=" + escala : "") + (teacher ? "&teacher_id=" + teacher : "") + (turma ? "&turma_id=" + turma : "");
	    
    });
    
    $('#select_year').change(function(){
	   var month = $("#select_month").val();
	   var year = $(this).val();
	   var escala = $(this).data('escala');
	   var teacher = $('#teacher_id').val();
	   var turma = $('#select_turma_id').val();
	   
	   window.location.href = "?month="+month+"&year="+year + (escala ? "&escala=" + escala : "") + (teacher ? "&teacher_id=" + teacher : "") + (turma ? "&turma_id=" + turma : "");
	    
    });
	
	//$('input[name="dias_manha"], input[name="dias_tarde"], input[name="dias_noite"], input[name="dias_ead"]').multiDatesPicker();
	
	/*$('#date_dias_manha, #date_dias_tarde, #date_dias_noite, #date_dias_ead').multiDatesPicker({
		onSelect: function(date) {
           var dates = $('#date_dias_manha').multiDatesPicker('getDates');
		   var dates_joined = dates.join(); 
		   $('input[name="dias_manha"]').val(dates_joined);
		   console.log('getDates()');
        }
	});*/
	

	
	$('input.dias_manha_hidden').multiDatesPicker({
		showOn: "button",
		buttonText: "Mostrar Dias",
		onSelect: function(date) {
		   var teacher_id = $(this).data('teacher-id');
           var dates = $(this).multiDatesPicker('getDates');
		   var dates_joined = dates.join(); 
		   
		   $.post('actions.php', { teacher_id: teacher_id, periodo: 'dias_manha', dias: dates_joined, act: 'save-teacher-dates' }, function(data){
			   //alert(data);
		   });
		   
       }
	}).attr('readonly', 'readonly');	
	$('input.dias_tarde_hidden').multiDatesPicker({
		showOn: "button",
		buttonText: "Mostrar Dias",
		onSelect: function(date) {
		   var teacher_id = $(this).data('teacher-id');
           var dates = $(this).multiDatesPicker('getDates');
		   var dates_joined = dates.join(); 
		   
		   $.post('actions.php', { teacher_id: teacher_id, periodo: 'dias_tarde', dias: dates_joined, act: 'save-teacher-dates' }, function(data){
			   //alert(data);
		   });
		   
       }
	});
	$('input.dias_noite_hidden').multiDatesPicker({
		showOn: "button",
		buttonText: "Mostrar Dias",
		onSelect: function(date) {
		   var teacher_id = $(this).data('teacher-id');
           var dates = $(this).multiDatesPicker('getDates');
		   var dates_joined = dates.join(); 
		   
		   $.post('actions.php', { teacher_id: teacher_id, periodo: 'dias_noite', dias: dates_joined, act: 'save-teacher-dates' }, function(data){
			   //alert(data);
		   });
		   
       }
	});
	$('input.dias_ead_hidden').multiDatesPicker({
		showOn: "button",
		buttonText: "Mostrar Dias",
		onSelect: function(date) {
		   var teacher_id = $(this).data('teacher-id');
           var dates = $(this).multiDatesPicker('getDates');
		   var dates_joined = dates.join(); 
		   
		   $.post('actions.php', { teacher_id: teacher_id, periodo: 'dias_ead', dias: dates_joined, act: 'save-teacher-dates' }, function(data){
			   //alert(data);
		   });
		   
       }
	});
	
	$('.test').click(function(){
		$(this).prev().trigger('focus');
	});
	
	var dias_manha_str 	= $('input[name="dias_manha"]').val();
	var dias_tarde_str 	= $('input[name="dias_tarde"]').val();
	var dias_noite_str 	= $('input[name="dias_noite"]').val();
	var dias_ead_str	= $('input[name="dias_ead"]').val();
	
	if(dias_manha_str) {
		var dias_manha 	= dias_manha_str.split(',');
		console.log(dias_manha);
	}
	
	if(dias_tarde_str) {
		var dias_tarde 	= dias_tarde_str.split(',');
	}
	
	if(dias_noite_str) {
		var dias_noite 	= dias_noite_str.split(',');
	}
	
	if(dias_ead_str) {
		var dias_ead 	= dias_ead_str.split(',');
	}
	
	
	
	
	
	
	
	$('#date_dias_manha').multiDatesPicker({
		onSelect: function(date) {
           var dates = $('#date_dias_manha').multiDatesPicker('getDates');
		   var dates_joined = dates.join(); 
		   $('input[name="dias_manha"]').val(dates_joined);
        }
	});
	
	$('#date_dias_tarde').multiDatesPicker({
		onSelect: function(date) {
           var dates = $('#date_dias_tarde').multiDatesPicker('getDates');
		   var dates_joined = dates.join(); 
		   $('input[name="dias_tarde"]').val(dates_joined);
        }
	});
	
	$('#date_dias_noite').multiDatesPicker({
		onSelect: function(date) {
           var dates = $('#date_dias_noite').multiDatesPicker('getDates');
		   var dates_joined = dates.join(); 
		   $('input[name="dias_noite"]').val(dates_joined);
        }
	});
	
	$('#date_dias_ead').multiDatesPicker({
		onSelect: function(date) {
           var dates = $('#date_dias_ead').multiDatesPicker('getDates');
		   var dates_joined = dates.join(); 
		   $('input[name="dias_ead"]').val(dates_joined);
        }
	});
	
	$.each(dias_manha, function( index, value ) {
	  $('#date_dias_manha').multiDatesPicker('addDates', value);
	});
	
	$.each(dias_tarde, function( index, value ) {
	  $('#date_dias_tarde').multiDatesPicker('addDates', value);
	});
	
	$.each(dias_noite, function( index, value ) {
	  $('#date_dias_noite').multiDatesPicker('addDates', value);
	});
	
	$.each(dias_ead, function( index, value ) {
	  $('#date_dias_ead').multiDatesPicker('addDates', value);
	});
		
	
});
