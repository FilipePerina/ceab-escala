<?php 
	require_once('classes/clsprofessores.php');
	require_once('classes/clsturmas.php');
	require_once('include/functions.php');
	$ObjProfessor = new professores;
	$ObjTurma = new turmas();
	$dia = RecebeParametro('dia');
	//$dia = 1;
	$turma = RecebeParametro('turma');
	$ArrProfessor = $ObjProfessor->Listar('','nome asc','');
	if(is_array($ArrProfessor)){
		echo "<option value='0' selected>Escolha um professor</option>";
		foreach($ArrProfessor as $row){
			if($turma){
				$ArrTurmas = $ObjTurma->Listar('id='.$turma,'','');
				if(is_array($ArrTurmas)){
					if($ArrTurmas[0]['periodo'] =='Manhã')
						$dias_disponiveis = explode(",", $row['dias_manha']);
					elseif($ArrTurmas[0]['periodo'] =='Tarde')
						$dias_disponiveis = explode(",", $row['dias_tarde']);
					elseif($ArrTurmas[0]['periodo'] =='Noite')
						$dias_disponiveis = explode(",", $row['dias_noite']);
					elseif($ArrTurmas[0]['periodo'] =='Sabado Manhã')
						$dias_disponiveis = explode(",", $row['dias_sabadoM']);
					elseif($ArrTurmas[0]['periodo'] =='Sabado Tarde')
						$dias_disponiveis = explode(",", $row['dias_sabadoT']);
					elseif($ArrTurmas[0]['periodo'] =='EAD')
						$dias_disponiveis = explode(",", $row['dias_ead']);
				}
			}
			for ($i=0; $i<count($dias_disponiveis); $i++) { 
				if($dia  == $dias_disponiveis[$i]){
					echo "<option value='".$row['id']."'>".$row['nome']."</option>";
				}
			}
		}
	}	
?>