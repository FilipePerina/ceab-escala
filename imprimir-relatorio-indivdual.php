<?php
	session_start();
	if ($_SESSION['logado'] != 'S') {
		header('Location: index.php');
	}

	$reportType = $_GET["escala"];

	if($reportType == "comissario"){
		require_once('classes/clsescalacomissario.php');
		$ObjEscala = new escalacomissario;
	}else{
		require_once('classes/clsescalacheckin.php');
		$ObjEscala = new escalacheckin;
	}

	require_once('include/functions.php');
	require_once('classes/clsprofessores.php');
	$ObjProfessor = new professores;

	$idProfessor = RecebeParametro('id');
	$ArrProfessor = $ObjProfessor->Listar('id='.$idProfessor, '','');
	if(is_array($ArrProfessor)){
		$nomeProfessor = $ArrProfessor[0]['nome'];
		$idProfessor = $ArrProfessor[0]['id'];
	}
	$str = RecebeParametro('str');
	if($str == 'periodo'){
		$mes = RecebeParametro('mes');
		$ano = $_GET['ano'];


		if($mes == 1){
			 $dias=31;
			 $nome="Janeiro";
		 }
		 if($mes == 2){
			 $dias=28;
			 $nome="Fevereiro";
		 }
		 if($mes == 3){
			 $dias=31;
			 $nome="Março";
		 }
		 if($mes == 4){
			 $dias=30;
			 $nome="Abril";
		 }
		 if($mes == 5){
			 $dias=31;
			 $nome="Maio";
		 }
		 if($mes == 6){
			 $dias=30;
			 $nome="Junho";
		 }
		 if($mes == 7){
			 $dias=31;
			 $nome="Julho";
		 }
		 if($mes == 8){
			 $dias=31;
			 $nome="Agosto";
		 }
		 if($mes == 9){
			 $dias=30;
			 $nome="Setembro";
		 }
		 if($mes == 10){
			 $dias=31;
			 $nome="Outubro";
		 }
		 if($mes == 11){
			 $dias=30;
			 $nome="Novembro";
		 }
		 if($mes == 12){
			 $dias=31;
			 $nome="Dezembro";
		 }


	}else{
		$mes = date('m') + 1;
		$ano = date('Y');

		if($mes == 1){
			 $dias=31;
			 $nome="Janeiro";
		 }
		 if($mes == 2){
			 $dias=28;
			 $nome="Fevereiro";
		 }
		 if($mes == 3){
			 $dias=31;
			 $nome="Março";
		 }
		 if($mes == 4){
			 $dias=30;
			 $nome="Abril";
		 }
		 if($mes == 5){
			 $dias=31;
			 $nome="Maio";
		 }
		 if($mes == 6){
			 $dias=30;
			 $nome="Junho";
		 }
		 if($mes == 7){
			 $dias=31;
			 $nome="Julho";
		 }
		 if($mes == 8){
			 $dias=31;
			 $nome="Agosto";
		 }
		 if($mes == 9){
			 $dias=30;
			 $nome="Setembro";
		 }
		 if($mes == 10){
			 $dias=31;
			 $nome="Outubro";
		 }
		 if($mes == 11){
			 $dias=30;
			 $nome="Novembro";
		 }
		 if($mes == 12){
			 $dias=31;
			 $nome="Dezembro";
		 }
	}
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<title>Sistema de Escala - CEAB -Brasil</title>
	<link rel="stylesheet" href="css/css.css" />
	<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
	<style>
		body{background-color: #FFF;}
		h1{color:#000;}

	</style>
	<script>
	$(document).ready(function() {
	    $('a#imprimir').click(function() {
	    window.print();
	    return false;
	    });
    });
	</script>
</head>
<body>
	<section class="alinha960">
		<h1>Relatório Individual</h1>
		<span class="botaoadicionar"><a href="#" id="imprimir"><img src="images/imprimir-relatorio.png" alt=""></a></span>
	 <form action="imprimir-relatorio-indivdual.php?id=<?php echo $idProfessor; ?>&str=periodo&escala=<?php echo $_GET['escala']; ?>" method="GET">
	 	<input type="hidden" name="id" value="<?php echo $idProfessor; ?>" />
	 	<input type="hidden" name="str" value="periodo" />
	 	<input type="hidden" name="escala" value="<?php echo $_GET['escala']; ?>" />
	 	<table>
	 	<tr>
			<td>Selecione o <strong>Mês</strong> e o <strong>Ano</strong>:</td>
			<td>
				<select name="mes" id="">
					<option value="">Selecione</option>
					<option value="1"<?php if ($_GET['mes'] == '1') { echo " selected"; } ?>>Janeiro</option>
					<option value="2"<?php if ($_GET['mes'] == '2') { echo " selected"; } ?>>Fevereiro</option>
					<option value="3"<?php if ($_GET['mes'] == '3') { echo " selected"; } ?>>Março</option>
					<option value="4"<?php if ($_GET['mes'] == '4') { echo " selected"; } ?>>Abril</option>
					<option value="5"<?php if ($_GET['mes'] == '5') { echo " selected"; } ?>>Maio</option>
					<option value="6"<?php if ($_GET['mes'] == '6') { echo " selected"; } ?>>Junho</option>
					<option value="7"<?php if ($_GET['mes'] == '7') { echo " selected"; } ?>>Julho</option>
					<option value="8"<?php if ($_GET['mes'] == '8') { echo " selected"; } ?>>Agosto</option>
					<option value="9"<?php if ($_GET['mes'] == '9') { echo " selected"; } ?>>Setembro</option>
					<option value="10"<?php if ($_GET['mes'] == '10') { echo " selected"; } ?>>Outubro</option>
					<option value="11"<?php if ($_GET['mes'] == '11') { echo " selected"; } ?>>Novembro</option>
					<option value="12"<?php if ($_GET['mes'] == '12') { echo " selected"; } ?>>Dezembro</option>
				</select>
			</td>
			<td><input type="text" name="ano" value="<?php if ($_GET['ano']) { echo $_GET['ano']; } else { echo date('Y'); } ?>"></td>
			<td><input type="submit" value="Gerar Relatório"></td>
		</tr>

		</table>
	 	</form>
		 <table width="100%" name="tabusu" class="tabelaconteudo">
	 		<thead>
	 			<tr>
	 				<td align="left"><img src="images/logo-ceab.png" alt=""></td>
	 				<td colspan="3" align="left">Instrutor: <?php echo $nomeProfessor;?></td>
	 			</tr>
	 			<tr>
	 				<td>Data</td>
	 				<td>Turma</td>
	 				<td>Matéria</td>
	 				<td>Horário</td>
	 			</tr>
	 		</thead>
	 		<tbody>
				<?php
					$ObjEscala->id_professor = $idProfessor;
					$ArrRelatorioProf = $ObjEscala->ListarDiasProfessor($dias, $mes, $ano);
					if(is_array($ArrRelatorioProf)){
						foreach($ArrRelatorioProf as $row){
				 ?>
	 			<tr>
	 				<td><?php echo $row['dataaula']; ?></td>
	 				<td><?php echo $row['nometurma']; ?></td>
	 				<td><?php echo $row['nomemateria']; ?></td>
	 				<td>
	 					<?php
	 						switch ($row['periodoaula']) {
								case 'Manha':
									echo '09:00 AS 12:00';
									break;

								case 'Tarde';
									echo '14:00 AS 17:00';
									break;

								case 'Noite';
									echo '19:30 as 22:00';
									break;

								case 'Sabado Manhã';
									echo '09:00 AS 12:30';
									break;

								case 'Sabado Tarde';
									echo '13:30 AS 17:00';
									break;

								case 'EAD';
									echo '19:30 AS 22:00';
									break;
							}
	 					 ?>

	 				</td>
	 			</tr>
	 			<?php
	 					}
	 				}else
	 				echo '<td colspan="4" align="center"> Nenhum registro encontrado.</td>';
	 			 ?>
	 		</tbody>
		 	</table>

	</section>
</body>
</html>
