<?php 
	session_start();
	if ($_SESSION['logado'] != 'S') {
		header('Location: index.php');
	}
require 'core.php';
require_once('include/functions.php');
require_once('classes/clsturmas.php');
require_once('classes/clsescalacomissario.php');
$ObjTurmas = new turmas;
$ObjEscalaComissario = new escalacomissario;
$str = RecebeParametro('str');
if($str =='incluir'){
	$ObjEscalaComissario->id_turma = RecebeParametro('turma');
	$ArrTurma = $ObjTurmas->Listar('id ='.$ObjEscalaComissario->id_turma,'','');
	if(is_array($ArrTurma))
		$ObjEscalaComissario->periodo = $ArrTurma[0]['periodo'];
	if($mes == 12){
	 	$ano=date('Y')+1;
	 	$mes = 1;
	 }else{
	 	$ano=date('Y');
	 	$mes=date('m') + 1;
	 }
	$ObjEscalaComissario->data_aula = $ano.'-'.$mes.'-'.limpalixo($_POST['dia']);
	$ObjEscalaComissario->id_professor = RecebeParametro('professor');
	$ObjEscalaComissario->id_materia = RecebeParametro('materia');
	$ObjEscalaComissario->data_insercao =  date('Y-m-d');
	$ObjEscalaComissario->Inserir();
	echo'<script>alert("Dados Salvos !"); location.href="escala-comissario.php";</script> ';
}elseif($str ==='deletardia'){
	$dia = RecebeParametro('dia');
	echo'<script>
		var a = confirm("Deseja deletar as aulas do dia '.dateFormatBrazil($dia).' ?");
		if(a === true)
			location.href="escala-comissario.php?str=deletar&dia='.$dia.'";	
	</script>';
}elseif($str=='deletar'){
	$ObjEscalaComissario->data_aula = RecebeParametro('dia');
	$ObjEscalaComissario->DeletarDia();
	echo'<script>alert("Dia excluido !"); location.href="escala-comissario.php";</script> ';
}
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<title>Sistema de Escala - CEAB -Brasil</title>
	<link rel="stylesheet" href="css/css.css" />
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript">
	function getValor(valor , valorturma){
		$("#recebeProfessor").html("<option value='0'>Carregando...</option>");
		setTimeout(function(){
		$("#recebeProfessor").load("ajaxValor.php",{dia:valor , turma:valorturma})
		}, 1000)
	};
	function getProfessor(valor, valorturma){
		$("#recebeMateria").html("<option value='0'>Carregando...</option>");
		setTimeout(function(){
		$("#recebeMateria").load("ajaxMateria.php",{idprofessor:valor , turma:valorturma})
		}, 1000)
	};
</script>
</head>
<body class="create-schedule" data-schedule='1'>
<?php include 'topo.php'; ?>
	<section class="alinha960">
		<h1><img src="images/setas.png" alt=""> Criar - Escala Comissário de Voo</h1>
		<span class="botaoadicionar"><a href="imprimir-pdf-escala-comissario.php" id="imprimir" target="_blank"><img src="images/imprimir-escala.png" alt=""></a></span>
	 <form action="escala-comissario.php?str=incluir" method="post">
		 <table width="100%" name="tabusu" class="tabelaconteudo">
	 		<tbody>
	 			<tr>
	 				<td>Turma:</td>
	 				<td>
	 				<select name="turma" id="turma">
	 					<option value="" selected>Selecione a Turma</option>
						<?php
						$dataHj = date('Y-m-d');
						$where = 'tipo = 1 and data_final >="'.$dataHj.'"';
						$ArrTurma = $ObjTurmas->Listar($where,'nome asc','');
						if(is_array($ArrTurma)){
							foreach($ArrTurma as $row){
						?>
	 					<option value="<?php echo $row['id']?>"><?php echo $row['nome'] .' - '. $row['periodo'].' - SALA ' . $row['sala'];?></option>
	 					<?php 
	 						}
	 					}
	 					?>
	 				</select>
	 				</td>
	 			</tr>
	 			
	 			<tr>
	 				<td>Turma:</td>
	 				<td>
	 				<select id="classes">
	 					<option value="" selected>Selecione a Turma</option>
	 				</select>
	 				</td>
	 			</tr>
	 			
	 			<tr>
	 				<td>Dia:</td>
	 				<td><input type="text" name="dia" id="dia" OnKeyUp="getValor(this.value, $('#turma').val());"></td>
	 			</tr>
	 			<tr>
	 				<td>Professor:</td>
	 				<td><select name="professor" id="recebeProfessor" onchange="getProfessor(this.value,$('#turma').val());">
	 					<option value="">Insira um dia</option>
	 				</select>
	 			</td>
	 			</tr>
	 			<tr>
	 				<td>Matéria:</td>
	 				<td><select name="materia" id="recebeMateria">
	 					<option value="">Selecione um Professor</option>
	 				</select></td>
	 			</tr>
	 			<tr>
	 				<td colspan="2" align="center"><input type="submit" value="Gravar"></td>
	 			</tr>
	 		</tbody>
	 	</table>		
 	</form>
	 
	 <div class="teacher-list" id="teacher-list">
		 <?php
			
			$m = $_SESSION['mysql']; 
					 
			$result = $m->select(array(
				'table' => 'professores',
			));
			
			print_r($result);		
				
			for($i = 0; $i < count($result); $i++){
				$teacher = $result[$i]['professores'];
				
				$name 		= $teacher['nome'];
				$morning 	= $teacher['dias_manha']; 
				$afternoon 	= $teacher['dias_tarde'];
				$night		= $teacher['dias_noite'];
			?>
				<div class="teacher" data-morning="" data-afternoon="" data-night=""><img src="http://placehold.it/100x100"><br><span><?php echo $name ?></span></div>
			<?php
			}
			?>
	 </div>
	 
	</section>
	<section style="float: left; width:100%;">
<?php
	 $mes=date('m');

	 if($mes == 12){
	 	$ano=date('Y')+1;
	 	$mes = 1;
	 }else{
	 	$ano=date('Y');
	 	$mes=date('m') + 1;
	 }

	 if($mes == 1){
		 $dias=31;
		 $nome="Janeiro";
	 }
	 if($mes == 2){
		 $dias=28;
		 $nome="Fevereiro";
	 }
	 if($mes == 3){
		 $dias=31;
		 $nome="Março";
	 }
	 if($mes == 4){
		 $dias=30;
		 $nome="Abril";
	 }
	 if($mes == 5){
		 $dias=31;
		 $nome="Maio";
	 }
	 if($mes == 6){
		 $dias=30;
		 $nome="Junho";
	 }
	 if($mes == 7){
		 $dias=31;
		 $nome="Julho";
	 }
	 if($mes == 8){
		 $dias=31;
		 $nome="Agosto";
	 }
	 if($mes == 9){
		 $dias=30;
		 $nome="Setembro";
	 }
	 if($mes == 10){
		 $dias=31;
		 $nome="Outubro";
	 }
	 if($mes == 11){
		 $dias=30;
		 $nome="Novembro";
	 }
	 if($mes == 12){
		 $dias=31;
		 $nome="Dezembro";
	 }
	?>
	<?php
	 echo '<br/> <h1>'.$nome . " de " . $ano.'</h1> <br/>';
	?>
	<table width="100%" border="0" height="100%" class="big-calendar" style="">
	<tr>
	<td align="center" width="3%">DOMINGO</td>
	<td align="center" width="15%">SEGUNDA-FEIRA</td>
	<td align="center" width="15%">TERÇA-FEIRA</td>
	<td align="center" width="15%">QUARTA-FEIRA</td>
	<td align="center" width="15%">QUINTA-FEIRA</td>
	<td align="center" width="15%">SEXTA-FEIRA</td>
	<td align="center" width="15%">SÁBADO</td>
	</tr>
	<?php
	 echo "<tr>";
	 for($i=1;$i<=$dias;$i++) {
		 $diadasemana = date("w",mktime(0,0,0,$mes,$i,$ano));
		 $cont = 0;
		 if($i == 1) {
			 while($cont < $diadasemana) {
				 echo "<td></td>";
				 $cont++;
			 }
	 	}
		 echo "<td class='day-item' height='250' valign='top' data-day='". $i ."' data-month='". date('m') ."' data-year='". date('Y') ."' >";
		 echo '<span style="float:right;"><a href="escala-comissario.php?str=deletardia&dia='.$ano.'-'.$mes.'-'.$i.'" style="text-decoration:none; color:#000;">'.$i.'</a></span><br/>';
		 	$ObjEscalaComissario->data_aula =  $ano.'-'.$mes.'-'.$i;
		 	$ArrEscalaComissario = $ObjEscalaComissario->ListarEscalaDiaria();
		 	if(is_array($ArrEscalaComissario)){
		 		foreach($ArrEscalaComissario as $row){
		 			echo '<span style="text-align:center; font-size:14px; font-weight:bold; padding-left:5px"><font color="#'.$row['cordaturma'].'" >'.$row['nometurma'].' - '.$row['siglamateria'].' - '.$row['nomeprofessor']. ' - SALA '.$row['salaturma'].'</font></span><br/>';
		 		}
		 	}
		 ?>
		 
		 	<div class="period morning">
			 	
		 	</div>
		 	<div class="period afternoon">
			 	
		 	</div>
		 	<div class="period night">
			 	
		 	</div>
		 
		 <?php	
		 echo "</td>";
		 if($diadasemana == 6) {
		 echo "</tr>";
		 echo "<tr>";
		 }
	 }
	 echo "</tr>";
?>
</table>
	</section>

	<br style="clear:both;">

	<?php require_once('rodape.php'); ?>
</body>
</html>