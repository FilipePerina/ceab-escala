<?php
	require "core.php";	
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<title>Sistema de Escala - CEAB -Brasil</title>
	
	<link rel="apple-touch-icon" sizes="57x57" href="favicons/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="favicons/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="favicons/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="favicons/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="favicons/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="favicons/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="favicons/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="favicons/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="favicons/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
	<link rel="manifest" href="favicons/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="favicons/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	
	<link rel="stylesheet" href="css/css.css" />
	<link rel="stylesheet" href="css/css-print.css" media="print" />
	
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	
</head>
<body class="create-schedule" data-schedule='1'>
<header class="topointerna">
	<section class="alinha960">
		<div class="logo">
			<a href="home.php"><img src="images/logo-ceab.png" alt=""></a>
			<p>Sistema de Escalas</p>
		</div>
		<ul class="menu">
			<?php if($_SESSION['type'] === 'admin' || $_SESSION['menu_access'] == 'admin'){ ?>
			<li><a href="lista-turmas.php"><img src="images/turmas.png" alt=""></a></li>
			<li><a href="lista-materias.php"><img src="images/materias.png" alt=""></a></li>
			<li><a href="lista-professores.php"><img src="images/professor.png" alt=""></a></li>
			<li><a href="escala.php"><img src="images/escala.png" alt=""></a></li>
			<li><a href="relatorios.php"><img src="images/relatorio.png" alt=""></a></li>
			<li><a href="sair.php"><img src="images/sair.png" alt=""></a></li>
			<?php }else{ ?>
			<li style="margin-left: 520px;"><a href="sair.php"><img src="images/sair.png" alt=""></a></li>
			<?php } ?>
		</ul>
	</section>
</header>