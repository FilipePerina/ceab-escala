<?php 
	session_start();
	if ($_SESSION['logado'] != 'S') {
		header('Location: index.php');
	}
require_once('include/functions.php');
require_once('classes/clsprofessores.php');
require_once("class.upload.php");
require_once("core.php");

$ObjProfessores = new professores;
$id = RecebeParametro('id');
if($id){
	$ObjProfessores->id = $id;
	$ObjProfessores->Excluir();
	echo('<script>alert("Registro Excluido com Sucesso !"); location.href="lista-professores.php";</script>');
}
	
	
	if($_POST['teacher_id']){
		$handle = new upload($_FILES['image_field']);
		if ($handle->uploaded) {
		  $handle->image_resize         = true;
		  $handle->image_x              = 200;
		  $handle->image_ratio_y        = true;
		  $handle->process('images/media/');
		  if ($handle->processed) {
		    
		    $m = $_SESSION['mysql'];
		    $teacher_id = $_POST['teacher_id'];
		    
		    $data = array(
		    	'foto' => $handle->file_dst_name,
		    );
		    $result = $m->update('professores',$data,"id = $teacher_id");
		    
		    $handle->clean();
		  } else {
		    echo 'error : ' . $handle->error;
		  }
		}	
	}
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<title>Sistema de Escala - CEAB -Brasil</title>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/css.css" />
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css" />
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jquery.quicksearch.js"></script>
	<script type="text/javascript">
	$(document).ready(function () {
			$('table#tabusu tbody tr').quicksearch({
				position: 'before',
				attached: 'table#tabusu',
				labelText: 'Pesquisar',
				loaderText: 'Pesquisando...',
				delay: 0
			});
			});
	</script>
</head>
<body>
<?php require_once('topo.php') ?>

	<section class="alinha960">
		<h1><img src="images/setas.png" alt=""> Listar Professores</h1>
		<span class="botaoadicionar"><a href="professores.php"><img src="images/novo-registro.png" alt=""></a></span>
	 <table width="100%" name="tabusu" class="tabelaconteudo" id="tabela-lista-professores">
	 		<thead>
	 			<tr>
	 				<td>Foto</td>
	 				<td>Nome do Professor</td>
	 				<td>Dias dísponiveis manhã</td>
	 				<td>Dias dísponiveis tarde</td>
	 				<td>Dias dísponiveis noite</td>
	 				<td>Dias dísponiveis EAD</td>
	 				<td>E-mail</td>
	 				<td>Matérias</td>
	 				<td>Ação</td>
	 				<?php if($_SESSION['user_id'] == 1){ ?>
	 				<td>Status</td>
	 				<?php } ?>
	 			</tr>
	 		</thead>
	 		<tbody>
	 			<?php  
	 				$ArrProfessores = $ObjProfessores->Listar('','nome ASC','');
	 				if(is_array($ArrProfessores)){
	 					foreach($ArrProfessores as $row){
	 						$ObjProfessores->id = $row['id'];
	 						$materias = $ObjProfessores->ListarMateriasAssociadas();
	 							 						
	 			?>
	 			<tr>
	 				<td>
		 				<a href="#" class="change_photo" data-id="<?php echo $row['id']; ?>">
		 					<img src="<?php echo ($row['foto'] ? "images/media/" . $row['foto'] : "images/sem-foto.png") ?>" width="50" />
		 				</a>
		 				<form enctype="multipart/form-data" method="post" action="lista-professores.php" data-id="<?php echo $row['id']; ?>">
			 				<input type="file" name="image_field" class="image_input" style="display: none;"  />
			 				<input type="hidden" name="teacher_id" value="<?php echo $row['id']; ?>" />
		 				</form>
		 			</td>
	 				<td><?php echo $row['nome']; ?></td>
	 				<td><input type="hidden" class="dias_manha_hidden" data-teacher-id="<?php echo $row['id']; ?>" value="<?php echo str_replace(",", ", ", str_replace(" ","",$row['dias_manha'])); ?>" /></td>
	 				<td><input type="hidden" class="dias_tarde_hidden" data-teacher-id="<?php echo $row['id']; ?>"  value="<?php echo str_replace(",", ", ", str_replace(" ","",$row['dias_tarde'])); ?>" /></td>
	 				<td><input type="hidden" class="dias_noite_hidden" data-teacher-id="<?php echo $row['id']; ?>"  value="<?php echo str_replace(",", ", ", str_replace(" ","",$row['dias_noite'])); ?>" /></td>
	 				<td><input type="hidden" class="dias_ead_hidden" data-teacher-id="<?php echo $row['id']; ?>"  value="<?php echo str_replace(",", ", ", str_replace(" ","",$row['dias_ead'])); ?>" /></td>
	 				<td><?php echo $row['email']; ?></td>
	 				<td>
	 					<?php 
	 						if($materias){
	 							foreach($materias as $rowmaterias)
	 								echo $rowmaterias['nomeMateria'] .', ';
	 						}
	 					 ?>
	 				</td>
	 				<td><a href="professores.php?id=<?php echo $row['id']; ?>"><img src="images/editar.png" alt=""><span>editar</span></a> <a href="lista-professores.php?str=excluir&id=<?php echo $row['id']; ?>"><img src="images/excluir.png" alt=""><span>excluir</span></a></td>
	 				<?php if($_SESSION['user_id'] == 1){
	 					$status = $row['status'];
	 				?>
	 				<td class="teacher-status"><a href="#" data-id="<?php echo $row['id']; ?>" data-status="<?php echo ($status == 1 ? 0 : 1); ?>" class="<?php echo ($status == 1 ? "green" : "red") ?>"><i class="fa fa-circle"></i></a></td>
	 				<?php } ?>
	 			</tr>
	 			<?php 
	 					}
	 				}
	 			 ?>
	 		</tbody>
	 	</table>	

	</section>

	<?php require_once('rodape.php'); ?>
</body>
</html>