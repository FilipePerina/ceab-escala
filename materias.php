<?php 
	session_start();
	if ($_SESSION['logado'] != 'S') {
		header('Location: index.php');
	}
require_once('include/functions.php');
require_once('classes/clsmaterias.php');
$ObjMaterias = new materias;
$id = RecebeParametro('id');
if($id){
	$acao ='alterar&id='.$id;
	$ArrMaterias = $ObjMaterias->Listar('id='.$id,'','');
	if($ArrMaterias){
		$nome = $ArrMaterias[0]['nome'];
		$sigla = $ArrMaterias[0]['sigla'];
		/*$quantidade_manha = $ArrMaterias[0]['quantidade_manha'];
		$quantidade_tarde = $ArrMaterias[0]['quantidade_tarde'];
		$quantidade_noite = $ArrMaterias[0]['quantidade_noite'];
		$quantidade_sabadoM = $ArrMaterias[0]['quantidade_sabadoM'];
		$quantidade_sabadoT = $ArrMaterias[0]['quantidade_sabadoT'];
		$quantidade_ead = $ArrMaterias[0]['quantidade_ead'];*/
	}
}else
	$acao = 'incluir';

$str =RecebeParametro('str');
if($str=='incluir'){
	$ObjMaterias->nome = RecebeParametro('nome');
	$ObjMaterias->sigla = RecebeParametro('sigla');
	/*$ObjMaterias->quantidade_manha = RecebeParametro('quantidade_manha');
	$ObjMaterias->quantidade_tarde = RecebeParametro('quantidade_tarde');
	$ObjMaterias->quantidade_noite = RecebeParametro('quantidade_noite');
	$ObjMaterias->quantidade_sabadoM = RecebeParametro('quantidade_sabadoM');
	$ObjMaterias->quantidade_sabadoT = RecebeParametro('quantidade_sabadoT');
	$ObjMaterias->quantidade_ead = RecebeParametro('quantidade_ead');*/
	$ObjMaterias->data_insercao = date('Y-m-d');
	$ObjMaterias->Inserir();
	echo'<script>alert("Dados Salvos !"); location.href="materias.php";</script> ';
}else if($str=='alterar'){
	$ObjMaterias->nome = RecebeParametro('nome');
	$ObjMaterias->sigla = RecebeParametro('sigla');
	/*$ObjMaterias->quantidade_manha = RecebeParametro('quantidade_manha');
	$ObjMaterias->quantidade_tarde = RecebeParametro('quantidade_tarde');
	$ObjMaterias->quantidade_noite = RecebeParametro('quantidade_noite');
	$ObjMaterias->quantidade_sabadoM = RecebeParametro('quantidade_sabadoM');
	$ObjMaterias->quantidade_sabadoT = RecebeParametro('quantidade_sabadoT');
	$ObjMaterias->quantidade_ead = RecebeParametro('quantidade_ead');*/
	$ObjMaterias->data_alteracao = date('Y-m-d');
	$ObjMaterias->id = $id;
	$ObjMaterias->Alterar();
	echo'<script>alert("Dados alterados com Sucesso !"); location.href="lista-materias.php";</script> ';
}
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<title>Sistema de Escala - CEAB -Brasil</title>
	<link rel="stylesheet" href="css/css.css" />
</head>
<body>
<?php require_once('topo.php') ?>
	<section class="alinha960">
		<h1><img src="images/setas.png" alt=""> Cadastrar Matérias</h1>
	 <form action="materias.php?str=<?php echo $acao; ?>" method="post">
		 <table width="100%" name="tabusu" class="tabelaconteudo">
	 		<tbody>
	 			<tr>
	 				<td>Nome da Matéria:</td>
	 				<?php  InputText('nome','','','','',$nome);?>
	 			</tr>
	 			<!--<tr>
	 				<td>Quantidade de Aulas Manhã: <strong>(Separar por virgulas ex: 1,2,3,4,5)</strong></td>
	 				<?php  //InputText('quantidade_manha','','','','',$quantidade_manha);?>
	 			</tr>
	 			<tr>
	 				<td>Quantidade de Aulas Tarde:<strong>(Separar por virgulas ex: 1,2,3,4,5)</strong></td>
	 				<?php  //InputText('quantidade_tarde','','','','',$quantidade_tarde);?>
	 			</tr>
	 			<tr>
	 				<td>Quantidade de Aulas Noite:<strong>(Separar por virgulas ex: 1,2,3,4,5)</strong></td>
	 				<?php  //InputText('quantidade_noite','','','','',$quantidade_noite);?>
	 			</tr>
	 			<tr>
	 				<td>Quantidade de Sabado Manhã:<strong>(Separar por virgulas ex: 1,2,3,4,5)</strong></td>
	 				<?php // InputText('quantidade_sabadoM','','','','',$quantidade_sabadoM);?>
	 			</tr>
	 			<tr>
	 				<td>Quantidade de Sabado Tarde:<strong>(Separar por virgulas ex: 1,2,3,4,5)</strong></td>
	 				<?php  //InputText('quantidade_sabadoT','','','','',$quantidade_sabadoT);?>
	 			</tr>
	 			<tr>
	 				<td>Quantidade EAD:<strong>(Separar por virgulas ex: 1,2,3,4,5)</strong></td>
	 				<?php  //InputText('quantidade_ead','','','','',$quantidade_ead);?>
	 			</tr>-->
	 			<tr>
	 				<td>Sigla da Matéria:</td>
	 				<?php  InputText('sigla','','','','',$sigla);?>
	 			</tr>
	 			<tr>
	 					<td colspan="2" align="center">
	 						<input type="submit" value="Gravar">
	 					</td>
	 				</tr>	
	 		</tbody>
		 	</table>
	 	</form>

	</section>

	<?php require_once('rodape.php'); ?>
</body>
</html>