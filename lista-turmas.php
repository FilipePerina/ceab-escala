<?php 
	session_start();
	if ($_SESSION['logado'] != 'S') {
		header('Location: index.php');
	}
require_once('include/functions.php');
require_once('classes/clsturmas.php');
$ObjTurmas = new turmas;
$id = RecebeParametro('id');
if($id){
	$ObjTurmas->id = $id;
	$ObjTurmas->Excluir();
	echo('<script>alert("Registro Excluido com Sucesso !"); location.href="lista-turmas.php";</script>');
}
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<title>Sistema de Escala - CEAB -Brasil</title>
	<link rel="stylesheet" href="css/css.css" />
	
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jquery.quicksearch.js"></script>
</head>
<body>
<?php require_once('topo.php') ?>
	<section class="alinha960">
		<h1><img src="images/setas.png" alt="">Listar Turmas</h1>
		<span class="botaoadicionar"><a href="turmas.php"><img src="images/novo-registro.png" alt=""></a></span>
	 <table width="100%" name="tabusu" id="tabusu" class="tabelaconteudo">
	 		<thead>
	 			<tr>
	 				<td>Nome da Turma</td>
	 				<td>Período</td>
	 				<td>Data de Início</td>
	 				<td>Data Final</td>
	 				<td>Tipo</td>
	 				<td>Sala</td>
	 				<td>Ação</td>
	 			</tr>
	 		</thead>
	 		<tbody>
	 			<?php  
	 				$ArrTurmas = $ObjTurmas->Listar('','nome ASC','');
	 				if(is_array($ArrTurmas)){
	 					foreach($ArrTurmas as $row){
	 			?>
	 			<tr>
	 				<td><?php echo $row['nome']; ?></td>
	 				<td><?php echo $row['periodo']; ?></td>
	 				<td><?php echo $row['datainicial']; ?></td>
	 				<td><?php echo $row['datafinal']; ?></td>
	 				<td><?php 
	 					if($row['tipo']==1) 
	 						echo "Comissário de Bordo";
	 					 else 
	 					 echo "Check-in"; ?>
	 				</td>
	 				<td><?php echo $row['sala'];?></td>
	 				<td><a href="turmas.php?id=<?php echo $row['id'];?>"><img src="images/editar.png" alt=""><span>editar</span></a> <a href="lista-turmas.php?str=excluir&id=<?php echo $row['id']; ?>"><img src="images/excluir.png" alt=""><span>excluir</span></a></td>
	 			</tr>
	 			<?php 
		 				}
		 			}
	 			 ?>
	 		</tbody>
	 	</table>	

	</section>

	<?php require_once('rodape.php'); ?>
</body>
</html>