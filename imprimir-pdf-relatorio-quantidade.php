<?php 
	session_start();
	if ($_SESSION['logado'] != 'S') {
		header('Location: index.php');
	}
	ob_start();
	require_once('include/functions.php');
	require_once('classes/clsprofessores.php');
	require_once('classes/clsescalacomissario.php');
	$ObjProfessor = new professores;
	$ObjEscala = new escalacomissario;
	
	$idProfessor = RecebeParametro('id');
	$ArrProfessor = $ObjProfessor->Listar('id='.$idProfessor, '','');
	if(is_array($ArrProfessor)){
		$nomeProfessor = $ArrProfessor[0]['nome'];
		$idProfessor = $ArrProfessor[0]['id'];
	}
	$str = RecebeParametro('str');
	if($str == 'periodo'){
		$mes = RecebeParametro('mes');
		$ano = RecebeParametro('ano');

		if($mes == 12){
		 	$ano=date('Y')+1;
		 	$mes = 1;
	 	}else
		 	$ano=date('Y');

		if($mes == 1){
			 $dias=31;
			 $nome="Janeiro";
		 }
		 if($mes == 2){
			 $dias=28;
			 $nome="Fevereiro";
		 }
		 if($mes == 3){
			 $dias=31;
			 $nome="Março";
		 }
		 if($mes == 4){
			 $dias=30;
			 $nome="Abril";
		 }
		 if($mes == 5){
			 $dias=31;
			 $nome="Maio";
		 }
		 if($mes == 6){
			 $dias=30;
			 $nome="Junho";
		 }
		 if($mes == 7){
			 $dias=31;
			 $nome="Julho";
		 }
		 if($mes == 8){
			 $dias=31;
			 $nome="Agosto";
		 }
		 if($mes == 9){
			 $dias=30;
			 $nome="Setembro";
		 }
		 if($mes == 10){
			 $dias=31;
			 $nome="Outubro";
		 }
		 if($mes == 11){
			 $dias=30;
			 $nome="Novembro";
		 }
		 if($mes == 12){
			 $dias=31;
			 $nome="Dezembro";
		 }
		 

	}else{
		$mes = date('m') + 1;
		$ano = date('Y');

		if($mes == 1){
			 $dias=31;
			 $nome="Janeiro";
		 }
		 if($mes == 2){
			 $dias=28;
			 $nome="Fevereiro";
		 }
		 if($mes == 3){
			 $dias=31;
			 $nome="Março";
		 }
		 if($mes == 4){
			 $dias=30;
			 $nome="Abril";
		 }
		 if($mes == 5){
			 $dias=31;
			 $nome="Maio";
		 }
		 if($mes == 6){
			 $dias=30;
			 $nome="Junho";
		 }
		 if($mes == 7){
			 $dias=31;
			 $nome="Julho";
		 }
		 if($mes == 8){
			 $dias=31;
			 $nome="Agosto";
		 }
		 if($mes == 9){
			 $dias=30;
			 $nome="Setembro";
		 }
		 if($mes == 10){
			 $dias=31;
			 $nome="Outubro";
		 }
		 if($mes == 11){
			 $dias=30;
			 $nome="Novembro";
		 }
		 if($mes == 12){
			 $dias=31;
			 $nome="Dezembro";
		 }
	}
	$html='<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<title>Sistema de Escala - CEAB -Brasil</title>
	<link rel="stylesheet" href="css/css-pdf.css" />
</head>
<body>
	<div class="alinha960">
		<h2>Relatório de quantidade de horas trabalhadas</h2>
		 <table width="100%" align="center" class="tabelaconteudo">
	 		<thead>
	 			<tr>
	 				<td align="left" width="40%"><img src="images/logo-ceab.png" alt=""></td>
	 			</tr>
	 			<tr>
					<td align="center">Instrutor: '. $nomeProfessor.'</td>
	 			</tr>
	 			<tr>
	 				<td align="Center" colspan="2">Quantidade de aulas no Mês de '.$nome.':</td>
	 			</tr>
	 		</thead>
	 		<tbody>';
	 		?>
				<?php 
					$ObjEscala->id_professor = $idProfessor;
					$ArrRelatorioProf = $ObjEscala->ListarDiasProfessor($dias, $mes, $ano);
					if(is_array($ArrRelatorioProf)){
						$quantidadeHoraAula = 0;
						foreach($ArrRelatorioProf as $row){
				 	 		switch ($row['periodoaula']) {
								case 'Manhã':
									$quantidadeHoraAula += 3;
									break;

								case 'Tarde';
									$quantidadeHoraAula += 3;
									break;
								
								case 'Noite';
									$quantidadeHoraAula += 3;
									break;

								case 'Sabado Manhã';
									$quantidadeHoraAula += 3.3;
									break;

								case 'Sabado Tarde';
									$quantidadeHoraAula += 3.3;
									break;

								case 'EAD';
									$quantidadeHoraAula += 3.3;
									break;
							}
						}
					}
							$html.='<tr style="background-color: #e3e3e3;"><td align="Center" colspan="2" style="ont-size: 12px; padding-left: 5px;">';
							$html.= $quantidadeHoraAula . ' Horas </td>';
	 				$html.='</tr>
	 		</tbody>
		 	</table>
	</div>
</body>
</html>';
include('html2pdf/html2pdf.class.php');
ob_start();
# Converte o html para pdf.
try
{
	/* Aqui estamos instanciando um novo objeto que irá criar o
	* pdf. Então vamos aos parametros passados:
	* 1º parâmetro: Utilize “P” para exibir o documento no
	* formato retrato e “L” para o formato
	* paisagem.
	* 2º parâmetro: Formato da folha A4, A5.......
	* 3º parâmetro: Caso ocorra alguma exceção durante a
	* conversão. Em qual idioma é para
	* exibir o erro. No caso o idioma escolhido
	* foi o português “pt”.
	* 4º parâmetro: Informe TRUE caso o html de entrada esteja
	* no formato unicode e FALSE caso negativo.
	* 5º parâmetro: Codificação a ser utilizada. ISO-8859-15, UTF-8 ......
	* 6º parâmetro: Margem do documento. Você pode informa um
	* único valor como no exemplo acima.
	* Outra forma é informa um array setando as
	* margens separadamente.: Exemplo:
	* $html2pdf = new HTML2PDF(
	* 'P',
	* 'A4',
	* 'pt',
	* false,
	* 'ISO-8859-15',
	* array(5,5,5,8));
	* Sendo que a primeira posição do array representa a margem esquerda depois
	* topo, direita e rodapé. */
	$html2pdf = new HTML2PDF('P','A4','pt', true, 'UTF-8', array(2,2,2,2));
	# Passamos o html que queremos converte.
	$html2pdf->writeHTML($html);
	/* Exibe o pdf:
	* 1º parãmetro: Nome do arquivo pdf. O nome que você quer dar ao pdf gerado.
	* 2º parâmetro: Tipo de saída:
	I: Abre o pdf gerado no navegador.
	D: Abre a janela para você realizar o download do pdf.
	F: Salva o pdf em alguma pasta do servidor. */
	$html2pdf->Output('relatorio-quantidade.pdf', 'I');
}
catch(HTML2PDF_exception $e)
{
	echo $e;
}
ob_flush();
ob_clean();
?>